import React, { forwardRef } from 'react';
import History from './History';
import HistoryComponentStyled, { CardOrder } from './styled';
import { FaShoppingBasket } from 'react-icons/fa';

const AdminMemberHistoryComponent = forwardRef((props, ref) => {
    return ( <
        HistoryComponentStyled >
        <
        History / >
        <
        /HistoryComponentStyled>
    );
});

export default AdminMemberHistoryComponent;