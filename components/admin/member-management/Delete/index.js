// import dayjs from 'dayjs';
// import numeral from 'numeral';
import HistoryStyled from './styled';
import * as React from 'react';
import Btn from '../../../shared/Button';
import Modal from '../../../shared/Modal';
import DeleteIcon from '@mui/icons-material/Delete'; // react hook form
import { useForm, Controller } from 'react-hook-form';
import { useState, useEffect } from 'react';

function MemberDeleteModal() {
    const [visible, setVisible] = useState(false);
    const {
        register,
        control,
        handleSubmit,
        formState: { errors },
    } = useForm({
        defaultValues: {
            firstName: '',
            lastname: '',
            idCard: '',
            phone: '',
            dayOfBirth: '',
        },
    });

    const onSubmit = (data) => {
        setVisible(false);
    };

    const handleAddMember = () => {
        console.log('handleAddMember !');
        setVisible(true);
    };
    const handelClose = () => {
        setVisible(false);
    };

    return (
        <HistoryStyled>
            <DeleteIcon className="text-black m-0 text-sm" onClick={handleAddMember} />
            <Modal visible={visible} handleClose={handelClose}>
                <div className="flex justify-center mb-2 text-xl">Confirm Delte?</div>
                <Btn type="submit" buttonClassName={'w-[100%] mt-4'} secondary>
                    บันทึก
                </Btn>
                <Btn type="submit" buttonClassName={'w-[100%] mt-4'} secondary>
                    ยกเลิก
                </Btn>
            </Modal>
        </HistoryStyled>
    );
}

export default MemberDeleteModal;
