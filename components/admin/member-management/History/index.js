// import dayjs from 'dayjs';
// import numeral from 'numeral';
import HistoryStyled from './styled';
import * as React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import SearchBar from 'material-ui-search-bar';
import { useState, useEffect } from 'react';
import Btn from '../../../shared/Button';
import Button from '@mui/material/Button';
import Modal from './../../../shared/Modal';
import Input from './../../../shared/Input';
import DatePickerMain from './../../../shared/DatePicker';
import MemberCreateModal from '../Update/index';
// MemberDeleteModal
import MemberDeleteModal from '../Delete/index';

// filter
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';

// react hook form
import { useForm, Controller } from 'react-hook-form';
import moment from 'moment';

const useStyles = makeStyles({
    table: {
        minWidth: 850,
    },
});

const USER_DATA = [
    {
        memberId: '1171',
        member_type: 'admin',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '2023-04-12T08:53:40.616Z',
        member_status: 'Active',
        upline_id: '', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1172',
        member_type: 'user',
        first_name: 'ค ควาย',
        last_name: 'ออกลูก',
        password: 'test1234!',
        card_id: 1,
        birth_date: '2023-04-12T08:53:40.616Z',
        member_status: 'Active',
        upline_id: '1171', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1173',
        member_type: 'user',
        first_name: 'จ จาน',
        last_name: 'ออกไข่',
        password: 'test1234!',
        card_id: 1,
        birth_date: '2023-04-12T08:53:40.616Z',
        member_status: 'Active',
        upline_id: '1171', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1174',
        member_type: 'user',
        first_name: 'ม ม้า',
        last_name: 'ออกไข่',
        password: 'test1234!',
        card_id: 1,
        birth_date: '2023-04-12T08:53:40.616Z',
        member_status: 'Active',
        upline_id: '1171', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1175',
        member_type: 'user',
        first_name: 'ข ไข่',
        last_name: 'ออกไข่',
        password: 'test1234!',
        card_id: 1,
        birth_date: '2023-04-12T08:53:40.616Z',
        member_status: 'Active',
        upline_id: '1171', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1176',
        member_type: 'user',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '2023-04-12T08:53:40.616Z',
        member_status: 'Active',
        upline_id: '1172', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1177',
        member_type: 'user',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '2023-04-12T08:53:40.616Z',
        member_status: 'Active',
        upline_id: '1173', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1178',
        member_type: 'user',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '2023-04-12T08:53:40.616Z',
        member_status: 'Active',
        upline_id: '1174', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1179',
        member_type: 'user',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '2023-04-12T08:53:40.616Z',
        member_status: 'Active',
        upline_id: '1174', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1180',
        member_type: 'user',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '2023-04-12T08:53:40.616Z',
        member_status: 'Active',
        upline_id: '1172', // ถ้าแอดมินไม่มี
        enable: true,
    },
];

const originalRows = [
    { name: 'Pizza', calories: 200, fat: 6.0, carbs: 24, protein: 4.0 },
    { name: 'Hot Dog', calories: 300, fat: 6.0, carbs: 24, protein: 4.0 },
    { name: 'Burger', calories: 400, fat: 6.0, carbs: 24, protein: 4.0 },
    { name: 'Hamburger', calories: 500, fat: 6.0, carbs: 24, protein: 4.0 },
    { name: 'Fries', calories: 600, fat: 6.0, carbs: 24, protein: 4.0 },
    { name: 'Ice Cream', calories: 700, fat: 6.0, carbs: 24, protein: 4.0 },
];

function History() {
    const [rows, setRows] = useState(originalRows);
    const [searched, setSearched] = useState('');
    const classes = useStyles();
    const [visible, setVisible] = useState(false);
    const [age, setAge] = React.useState('');

    const {
        register,
        control,
        handleSubmit,
        formState: { errors },
    } = useForm({
        defaultValues: {
            firstName: '',
            lastname: '',
            idCard: '',
            phone: '',
            dayOfBirth: '',
        },
    });

    const onSubmit = (data) => {
        setVisible(false);
    };

    const requestSearch = (searchedVal) => {
        const filteredRows = originalRows.filter((row) => {
            return row.name.toLowerCase().includes(searchedVal.toLowerCase());
        });
        setRows(filteredRows);
    };

    const cancelSearch = () => {
        setSearched('');
        requestSearch(searched);
    };

    const handleAddMember = () => {
        console.log('handleAddMember !');
        setVisible(true);
    };

    const handelClose = () => {
        setVisible(false);
    };

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    return (
        <HistoryStyled>
            <div className="flex flex-row justify-between">
                <div className="text-black text-xl  my-3">รายการสมาชิกในระบบ</div>
                <Button variant="contained" color="primary" onClick={handleAddMember} className="h-10 rounded-md">
                    <span className="p-3 text-primary">เพิ่มสมาชิก</span>
                </Button>
            </div>

       
            <Paper>
                <SearchBar placeholder='เบอร์โทร/id ccard/ รหัสสมาชิก' value={searched} onChange={(searchVal) => requestSearch(searchVal)} onCancelSearch={() => cancelSearch()} />
                <TableContainer>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Member ID</TableCell>
                                <TableCell align="center">Name</TableCell>
                                <TableCell align="center">Birth Date</TableCell>
                                <TableCell align="center">Upline ID</TableCell>
                                <TableCell align="center">Created At</TableCell>
                                <TableCell align="center">Member Status</TableCell>
                                <TableCell align="center">Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {USER_DATA.map((row) => (
                                <TableRow key={row.name}>
                                    <TableCell component="th" scope="row">
                                        {row.memberId}
                                    </TableCell>
                                    <TableCell align="center">{row.first_name}</TableCell>
                                    <TableCell align="center">{row.birth_date}</TableCell>
                                    <TableCell align="center">{row.upline_id || '-'}</TableCell>
                                    <TableCell align="center">{moment(row.birth_date).format('llll')}</TableCell>
                                    <TableCell align="center">{row.member_status}</TableCell>
                                    {/* icon edit, delete */}
                                    <TableCell align="center">
                                        <div className="flex items-center">
                                            <MemberCreateModal />
                                            <MemberDeleteModal />
                                        </div>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>

            <Modal visible={visible} handleClose={handelClose}>
                <div className="flex justify-center mb-2 text-xl">เพิ่มสายงาน</div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Controller
                        name="firstName"
                        {...register('firstName', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="ชื่อ*" error={errors.firstName} />}
                    />
                    <Controller
                        name="lastname"
                        {...register('lastname', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="นามสกุล*" error={errors.lastname} />}
                    />
                    <Controller
                        name="idCard"
                        {...register('idCard', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="รหัสบัตรประชาชน*" error={errors.idCard} />}
                    />
                    <Controller
                        name="phone"
                        {...register('phone', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="เบอร์โทรศัพท์*" error={errors.phone} />}
                    />
                    <Controller
                        name="dayOfBirth"
                        {...register('dayOfBirth', { required: false })}
                        control={control}
                        render={({ field }) => <DatePickerMain {...field} placeholder="วันเกิด" />}
                    />

                    <div className="flex justify-center ">
                        <Btn type="submit" buttonClassName={'w-[100%] mt-4'} secondary>
                            บันทึก
                        </Btn>
                    </div>
                </form>
            </Modal>
        </HistoryStyled>
    );
}

export default History;
