import React, { forwardRef } from 'react';
import History from './History';
import HistoryComponentStyled, { CardOrder } from './styled';

const AdminOrderHistoryComponent = forwardRef((props, ref) => {
    return (
        <HistoryComponentStyled>
            <History />
        </HistoryComponentStyled>
    );
});

export default AdminOrderHistoryComponent;
