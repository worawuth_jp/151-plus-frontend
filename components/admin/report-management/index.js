import React, { forwardRef } from 'react';
import History from './History';
import HistoryComponentStyled, { CardOrder } from './styled';

// รายการยอดขาย
const AdminReportHistoryComponent = forwardRef((props, ref) => {
    return (
        <HistoryComponentStyled>
            <History />
        </HistoryComponentStyled>
    );
});

export default AdminReportHistoryComponent;
