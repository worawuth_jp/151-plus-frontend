import React, { forwardRef } from 'react';
import History from './History';
import HistoryComponentStyled, { CardOrder } from './styled';

// รายการขอถอนเงิน
const AdminTransactionHistoryComponent = forwardRef((props, ref) => {
    return (
        <HistoryComponentStyled>
            <History />
        </HistoryComponentStyled>
    );
});

export default AdminTransactionHistoryComponent;
