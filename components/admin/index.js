import React, { forwardRef, useState } from 'react';
import IndexComponentStyled from './styled';
import { FaWallet, FaChevronDown, FaChevronUp, FaEye, FaCheckCircle, FaRegCircle } from 'react-icons/fa';
import numeral from 'numeral';
import { useRouter } from 'next/router';
import Modal from '../shared/Modal';
import { useForm, Controller } from 'react-hook-form';
import Btn from '../shared/Button';
import Input from '../shared/Input';


const AdminIndexComponent = forwardRef((props, ref) => {
    const [showMore, setShowMore] = useState(false);
    const [viewCardId, setViewCardId] = useState(false);
    const [balance, setBalance] = useState(15000);
    const [visible, setVisible] = useState(false);
    const router = useRouter();
    const {
        register,
        control,
        handleSubmit,
        formState: { errors },
    } = useForm({
        defaultValues: {
            firstName: '',
            lastname: '',
            idCard: '',
            phone: '',
            dayOfBirth: '',
        },
    });

    const onSubmit = (data) => {
        setVisible(false);
    };

    return (
        <IndexComponentStyled>
            {/* เดือนที่แล้ว */}
            <div className="card-details-previus">
                <div>
                    <div className="flex flex-row justify-end w-full point-gp">
                        <span>11,900</span>
                        <span className="ml-1">GP</span>
                    </div>
                </div>
                <div>
                    <div className="flex flex-row justify-start w-full name">
                        <span>Worawut</span>
                        <span className="ml-1">Panthusitseree</span>
                    </div>
                </div>

                <div>
                    <div className="flex flex-row justify-start w-full member-id">
                        <span>Member No. :</span>
                        <span className="ml-1">75001</span>
                    </div>
                </div>

                <div className="mt-3 label-balance">ยอดเงินคงเหลือของเดือนที่แล้ว</div>

                <div className="mb-1 flex flex-row">
                    <div className="flex flex-row justify-start items-center  balance">
                        <span>
                            <FaWallet color="green" size={36} />
                        </span>
                        <span className="ml-2">{numeral(222222).format('0,00.00')}</span>
                        <span className="ml-2 thb">THB</span>
                    </div>
                </div>
            </div>

            {/* admin menu */}
            <div className="card-details ">
                <div>
                    <div className="flex flex-row justify-end w-full point-gp">
                        <span>1900</span>
                        <span className="ml-1">GP</span>
                    </div>
                </div>
                <div>
                    <div className="flex flex-row justify-start w-full name">
                        <span>Admin</span>
                        <span className="ml-1">System</span>
                    </div>
                </div>

                <div>
                    <div className="flex flex-row justify-start w-full member-id">
                        <span>Member No. :</span>
                        <span className="ml-1">75001</span>
                    </div>
                </div>

                <div className="mt-3 label-balance">ยอดเงินคงเหลือ</div>

                <div className="mb-1 flex flex-row">
                    {/* แสดงรายได้ปุัจจุบัน */}
                    <div className="flex flex-row justify-start items-center  balance">
                        <span>
                            <FaWallet color="green" size={36} />
                        </span>
                        <span className="ml-2">{numeral(balance).format('0,00.00')}</span>
                        <span className="ml-2 thb">THB</span>
                    </div>
                    {/* {balance > 0 && (
                        <div className="flex flex-row justify-start items-center" onClick={() => setVisible(!visible)}>
                            <span className="ml-2 thb underline cursor-pointer">ขอถอนเงิน</span>
                        </div>
                    )} */}
                </div>

                <div className="mt-2">
                    <div className="flex flex-row items-center justify-between w-full status">
                        <span className="ml-1">
                            <span className="dot-status">•</span>
                            <span>Active</span>
                        </span>
                        <div className="flex flex-row items-center content-end">
                            <div className="more mb-auto" onClick={() => setShowMore((prev) => !prev)}>
                                ดูรายละเอียด
                                {!showMore ? <FaChevronDown size={10} className="mt-1" /> : <FaChevronUp size={10} className="mt-1" />}
                            </div>
                        </div>
                    </div>
                </div>

                {/* //show more */}
                {showMore && (
                    <div className="mt-4 show-more">
                        <div className="font-bold text-black mt-1">ยอดรวมบัญชี</div>
                        <div>
                            <div className="flex flex-row w-full">
                                <span>คะแนนรวม : </span>
                                <span className="ml-2">15,100</span>
                                <span className="ml-1">GP</span>
                                <span className="ml-1">( +900 GP )</span>
                            </div>
                        </div>

                        <div>
                            <div className="flex flex-row w-full">
                                <span>รายได้รวม : </span>
                                <span className="ml-2">185,001.00</span>
                                <span className="ml-1">THB</span>
                                <span className="ml-1">( +15,000 THB )</span>
                            </div>
                        </div>

                        <div>
                            <div className="flex flex-row w-full">
                                <span>จำนวนสมาชิก : </span>
                                <span className="ml-2">1,212</span>
                                <span className="ml-1">คน</span>
                                <span className="ml-1">( +12 คน )</span>
                            </div>
                        </div>

                        <div className="font-bold text-black mt-4">ข้อมูลส่วนตัว</div>
                        <div>
                            <div className="flex flex-row w-full">
                                <span>เลขบัตรประชาชน : </span>
                                <span className="ml-2">{viewCardId ? '1-7100-11223-129' : '1-7100-XXXXX-XX9'}</span>
                                <span className="ml-2 mt-[5px]">
                                    <FaEye size={14} className="cursor-pointer" onClick={() => setViewCardId((prev) => !prev)} />
                                </span>
                            </div>
                        </div>

                        <div>
                            <div className="flex flex-row w-full">
                                <span>วันเกิด : </span>
                                <span className="ml-2">09 พฤกษภาคม 2542</span>
                            </div>
                        </div>

                        <div>
                            <div className="flex flex-row w-full">
                                <span>เบอร์ติดต่อ : </span>
                                <span className="ml-2">092-5191680</span>
                            </div>
                        </div>

                        <div
                            className="font-bold cursor-pointer text-black mt-4"
                            onClick={() => {
                                router.push('/profile');
                            }}>
                            <span className="underline">อัพโหลดเอกสารเพิ่มเติม</span>
                            <span>(สำเนาบัตรประชาชนและบัญชีธนาคาร)</span>
                        </div>

                        <div className="font-bold text-black mt-4">
                            <span>ที่อยู่ในการจัดส่ง : </span>
                        </div>

                        <div>
                            <div className="flex flex-row w-full">
                                <span className="ml-0 break-all">
                                    11/12 ม.3 ต.ปากแพรก อ.เมือง จ.กาญจนบุรี 71110 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                                </span>
                            </div>
                        </div>

                        <div className="font-bold text-black mt-4">บัญชีธนาคาร</div>
                        <div>
                            <img className="w-10" src="https://f.ptcdn.info/801/022/000/1409170288-b60f8c1e0e-o.png" />
                            ธนาคารกรุงเทพ จำกัด (มหาชน)
                        </div>

                        <div>
                            <div className="flex flex-row w-full">
                                <span>เลขที่บัญชี : </span>
                                <span className="ml-2">1112540584</span>
                            </div>
                        </div>
                    </div>
                )}
            </div>

            {/* Profit Condition */}
            <div className="card-details">
                <div>
                    <div className="text-[#6A6A6A] font-semibold text-sm mb-1">สิทธิประโยชน์</div>
                    <div>
                        <div className="flex items-center">
                            <span>
                                <FaCheckCircle color="green" className="mr-1" />
                            </span>
                            <span>กำไรการขายสินค้า</span>
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center ">
                            <span>
                                <FaCheckCircle color="green" className="mr-1" />
                            </span>
                            <span>Commission</span>
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center disable">
                            <span>
                                <FaRegCircle color="gray" className="mr-1" />
                            </span>
                            <span>
                                Bonus 1 <span className="time">(7 วัน 20:34:32 )</span>
                            </span>
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center disable">
                            <span>
                                <FaRegCircle color="gray" className="mr-1" />
                            </span>
                            <span>
                                Bonus 2 <span className="time-warning">(37 วัน 20:34:32 )</span>
                            </span>
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center disable">
                            <span>
                                <FaRegCircle color="gray" className="mr-1" />
                            </span>
                            <span>
                                Bonus 3 <span className="time-danger">(67 วัน 20:34:32 )</span>
                            </span>
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center disable">
                            <span>
                                <FaRegCircle color="gray" className="mr-1" />
                            </span>
                            <span>Extra Commission +50%</span>
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center disable">
                            <span>
                                <FaRegCircle color="gray" className="mr-1" />
                            </span>
                            <span>Commission Lv.7</span>
                        </div>
                    </div>

                    <div>
                        <div className="flex items-center disable">
                            <span>
                                <FaRegCircle color="gray" className="mr-1" />
                            </span>
                            <span>All Sale (3-5%)</span>
                        </div>
                    </div>
                </div>
            </div>

      
            {/* modal ถอนเงิน */}
            <Modal visible={visible} handleClose={() => setVisible(false)}>
                <div className="flex justify-center mb-2 text-md underline">คำร้องขอถอนเงิน</div>

                <div>
                    <div className="font-bold text-black mt-4">ถอนเงินจากแอป ไปที่ บัญชีธนาคาร</div>
                    <div>
                        <img className="w-10 m-3" src="https://f.ptcdn.info/801/022/000/1409170288-b60f8c1e0e-o.png" />
                        ธนาคารกรุงเทพ จำกัด (มหาชน)
                    </div>

                    <div>
                        <div className="flex flex-row w-full">
                            <span>เลขที่บัญชี : </span>
                            <span className="ml-2">1112540584</span>
                        </div>
                    </div>
                </div>

                <div>
                    <div className="font-bold text-black mt-4">ยอดการถอนเงิน</div>

                    <form onSubmit={handleSubmit(onSubmit)}>
                        <Controller
                            name="amount"
                            {...register('amount', { required: false })}
                            control={control}
                            render={({ field }) => <Input {...field} className="w-full p-0" placeholder="ยอดเงิน*" error={errors.amount} />}
                        />

                        <div className="flex justify-center ">
                            <Btn type="submit" buttonClassName={'w-[100%] mt-4'} secondary>
                                บันทึก
                            </Btn>
                        </div>
                    </form>
                </div>
            </Modal>
        </IndexComponentStyled>
    );
});

export default AdminIndexComponent;
