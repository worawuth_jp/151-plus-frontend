// import dayjs from 'dayjs';
// import numeral from 'numeral';
import HistoryStyled from './styled';
import * as React from 'react';
import Btn from '../../../shared/Button';
import Button from '@mui/material/Button';
import Modal from '../../../shared/Modal';
import Input from '../../../shared/Input';
import DatePickerMain from '../../../shared/DatePicker';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete'; // react hook form
import { useForm, Controller } from 'react-hook-form';
import { useState, useEffect } from 'react';

function MemberCreateModal() {
    const [visible, setVisible] = useState(false);
    const {
        register,
        control,
        handleSubmit,
        formState: { errors },
    } = useForm({
        defaultValues: {
            firstName: '',
            lastname: '',
            idCard: '',
            phone: '',
            dayOfBirth: '',
        },
    });

    const onSubmit = (data) => {
        setVisible(false);
    };

    const handleAddMember = () => {
        console.log('handleAddMember !');
        setVisible(true);
    };
    const handelClose = () => {
        setVisible(false);
    };
    return (
        <HistoryStyled>
            <EditIcon className='text-black' onClick={handleAddMember} />

            <Modal visible={visible} handleClose={handelClose}>
                <div className="flex justify-center mb-2 text-xl">แก้ไขสมาชิก</div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Controller
                        name="firstName"
                        {...register('firstName', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="ชื่อ*" error={errors.firstName} />}
                    />
                    <Controller
                        name="lastname"
                        {...register('lastname', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="นามสกุล*" error={errors.lastname} />}
                    />
                    <Controller
                        name="idCard"
                        {...register('idCard', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="รหัสบัตรประชาชน*" error={errors.idCard} />}
                    />
                    <Controller
                        name="phone"
                        {...register('phone', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="เบอร์โทรศัพท์*" error={errors.phone} />}
                    />
                    <Controller
                        name="dayOfBirth"
                        {...register('dayOfBirth', { required: false })}
                        control={control}
                        render={({ field }) => <DatePickerMain {...field} placeholder="วันเกิด" />}
                    />

                    <div className="flex justify-center ">
                        <Btn type="submit" buttonClassName={'w-[100%] mt-4'} secondary>
                            บันทึก
                        </Btn>
                    </div>
                </form>
            </Modal>
        </HistoryStyled>
    );
}

export default MemberCreateModal;
