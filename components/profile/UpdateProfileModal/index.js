import React from 'react';
import UpdateProfileModalStyled from './styled';
import Btn from '../../shared/Button';
import Modal from '../../shared/Modal';
import Input from '../../shared/Input';
import DatePickerMain from '../../shared/DatePicker';

// react hook form
import { useForm, Controller } from 'react-hook-form';

function UpdateProfileModal({ visible, handelClose, onSubmit }) {
    const {
        register,
        control,
        handleSubmit,
        formState: { errors },
    } = useForm({
        defaultValues: {
            firstName: '',
            lastname: '',
            idCard: '',
            phone: '',
            dayOfBirth: '',
        },
    });

    return (
        <UpdateProfileModalStyled>
            <Modal visible={visible} handleClose={handelClose}>
                <div className="flex justify-center mb-2 text-xl">แก้ไขข้อมูลส่วนตัว</div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Controller
                        name="firstName"
                        {...register('firstName', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="ชื่อ*" error={errors.firstName} />}
                    />
                    <Controller
                        name="lastname"
                        {...register('lastname', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="นามสกุล*" error={errors.lastname} />}
                    />
                    <Controller
                        name="idCard"
                        {...register('idCard', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="รหัสบัตรประชาชน*" error={errors.idCard} />}
                    />
                    <Controller
                        name="phone"
                        {...register('phone', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="เบอร์โทรศัพท์*" error={errors.phone} />}
                    />
                    <Controller
                        name="dayOfBirth"
                        {...register('dayOfBirth', { required: false })}
                        control={control}
                        render={({ field }) => <DatePickerMain {...field} placeholder="วันเกิด" />}
                    />

                    <div className="flex justify-center ">
                        <Btn type="submit" buttonClassName={'w-[100%] mt-4'} secondary>
                            บันทึก
                        </Btn>
                    </div>
                </form>
            </Modal>
        </UpdateProfileModalStyled>
    );
}

export default UpdateProfileModal;
