import styled from 'styled-components';

const ProfileStyled = styled.div`
    background: ${(props) => props.bg};
    min-height: 50px;
    border-radius: 10px;
    color: #fff;
    padding: 1rem;
    cursor: pointer;
    :not(:first-child) {
        margin-top: 10px;
    }
`;

export default ProfileStyled;
