import React, { useState } from 'react';
import ProfileStyled from './styled';
import { Typography } from '@mui/material';

function Profile({ data, handelOpenUploadDocument }) {
    return (
        <ProfileStyled>
            <Typography variant="h5" color={'GrayText'} component="h2">
                ขื่อ : {data.first_name} {data.last_name}
            </Typography>
            <Typography variant="h5" color={'GrayText'} component="h2">
                อีเมล์ : {data.email}
            </Typography>
            <Typography variant="h5" color={'GrayText'} component="h2">
                บัตรประชาชน : {data.IdCard}
            </Typography>
            <Typography variant="h5" color={'GrayText'} component="h2">
                เบอร์โทร : {data.phone}
            </Typography>
            <Typography variant="h5" color={'GrayText'} component="h2">
                วันเกิด : {data.birth_date}
            </Typography>
            <Typography variant="h5" color={'GrayText'} component="h2">
                สถานะ : {data.member_status}
            </Typography>
            <Typography
                variant="subtitle1"
                color={'black'}
                component="h2"
                sx={{
                    textDecoration: 'underline',
                }}
                onClick={handelOpenUploadDocument}>
                อัพโหลด สำเนาบัตรประชาชน/ หน้าบัญชีธนาคาร
            </Typography>
        </ProfileStyled>
    );
}

export default Profile;
