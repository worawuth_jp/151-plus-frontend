import React, { useState } from 'react';
import UpdateProfileModalStyled from './styled';
import Modal from '../../shared/Modal';
import Image from 'next/image';

// react hook form
import { useForm, Controller } from 'react-hook-form';
import Btn from '../../shared/Button';

const UploadState = {
    IDLE: 1,
    UPLOADING: 2,
    UPLOADED: 3,
};

Object.freeze(UploadState);

function UpdateDocumentModal({ visible, handelClose, onSubmit }) {
    const [uploadState, setUploadState] = useState(UploadState.IDLE);
    const [imgUrlIdCard, setImgUrlIdCard] = useState('');
    const [imgUrlBank, setImgUrlBank] = useState('');

    const {
        register,
        control,
        handleSubmit,
        formState: { errors },
    } = useForm({
        defaultValues: {
            firstName: '',
            lastname: '',
            idCard: '',
            phone: '',
            dayOfBirth: '',
        },
    });

    async function handleFormData(e, key) {
        setUploadState(UploadState.UPLOADING);
        // const file = e.target.files[0];
        // const formData = new FormData();
        let url = URL.createObjectURL(e.target.files[0]);

        if (key === 'idCard') {
            setImgUrlIdCard(url);
        }

        if (key === 'bankAccount') {
            setImgUrlBank(url);
        }

        setUploadState(UploadState.UPLOADED);
    }

    return (
        <UpdateProfileModalStyled>
            <Modal visible={visible} handleClose={handelClose}>
                <div className="flex justify-start mb-2 text-md underline">อัพโหลดเอกสารเพิ่มเติม</div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="mt-5">
                        <span className="text-lg">สำเนาบัตรประชาชน</span>
                        {imgUrlIdCard && <Image className=" z-0 p-3 object-cover w-[100px]" id="idCard" src={imgUrlIdCard} width={130} height={130} alt="image" />}
                        <Controller
                            name="idCard"
                            {...register('idCard', { required: false })}
                            control={control}
                            render={({ field }) => (
                                <div className="p-10 border border-dashed mt-1">
                                    <input type="file" name="idCard" id="idCard" className="" onChange={(e) => handleFormData(e, 'idCard')} />
                                </div>
                            )}
                        />
                    </div>

                    <div className="mt-5">
                        <span className="text-lg">บัญชีธนาคาร</span>
                        {imgUrlBank && <Image className=" z-0 p-3 object-cover w-[100px]" id="bankAccount" src={imgUrlBank} width={130} height={130} alt="image" />}
                        <Controller
                            name="bankAccount"
                            {...register('bankAccount', { required: false })}
                            control={control}
                            render={({ field }) => (
                                <div className="p-10 border border-dashed mt-1">
                                    <input type="file" name="bankAccount" id="bankAccount" className="" onChange={(e) => handleFormData(e, 'bankAccount')} />
                                </div>
                            )}
                        />
                    </div>

                    <div className="flex justify-center ">
                        <Btn type="submit" buttonClassName={'w-[100%] mt-4'} secondary>
                            บันทึก
                        </Btn>
                    </div>
                </form>
            </Modal>
        </UpdateProfileModalStyled>
    );
}

export default UpdateDocumentModal;
