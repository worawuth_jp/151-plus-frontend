import React, { forwardRef, useState } from 'react';
import Profile from './Profile';
import ProfileComponentStyled, { CardProfile, CardProfileRow } from './styled';

// component
import UploadProfile from './Upload';
import Btn from '../shared/Button';
import UpdateProfileModal from './UpdateProfileModal';
import UpdateDocumentModal from './UpdateDocumentModal';

const data = {
    first_name: 'Jantapa',
    last_name: 'Binheem',
    email: 'jantapa@gmail.com',
    IdCard: '1234567899999',
    avatar: '/images/profile/me.jpg',
    phone: '0987654321',
    upline_id: 'R1234',
    member_status: 'active',
    birth_date: '2015-02-22',
    enable: true,
};

const UploadState = {
    IDLE: 1,
    UPLOADING: 2,
    UPLOADED: 3,
};

Object.freeze(UploadState);

const ProfileComponent = forwardRef((props, ref) => {
    const [uploadState, setUploadState] = useState(UploadState.IDLE);
    const [imgUrl, setImgUrl] = useState('');
    const [visible, setVisible] = useState(false);
    const [visibleModal, setVisibleModal] = useState(false);

    async function handleFormData(e) {
        setUploadState(UploadState.UPLOADING);
        const file = e.target.files[0];
        const formData = new FormData();
        // formData.append('file', file);
        // const res = await fetch('/api/upload', {
        //     method: 'POST',
        //     body: formData,
        // });
        // const data = await res.json();
        let url = URL.createObjectURL(e.target.files[0]);
        setImgUrl(url);
        setUploadState(UploadState.UPLOADED);
    }

    const onSubmit = (data) => {
        setVisible(false);
    };

    const handleAddMember = () => {
        setVisible(true);
    };

    const handelClose = () => {
        setVisible(false);
    };

    const handelOpenUploadDocument = () => {
        // setVisible(false);
        setVisibleModal(true)
    };

    const onSubmitUploadDuc = (data) => {
        setVisibleModal(false);
    };


    return (
        <ProfileComponentStyled>
            <CardProfile>
                <CardProfileRow>
                    <div className="underline font-bold">ข้อมูลส่วนตัว</div>
                    <Btn buttonClassName={'w-[100%]'} primary onClick={handleAddMember}>
                        แก้ไขข้อมูลส่วนตัว
                    </Btn>
                </CardProfileRow>
                <UploadProfile data={data} uploadData={imgUrl} onChange={handleFormData} />
                <Profile data={data} handelOpenUploadDocument={handelOpenUploadDocument} />
            </CardProfile>

            <UpdateProfileModal visible={visible} handelClose={handelClose} onSubmit={onSubmit}  />
            <UpdateDocumentModal visible={visibleModal} handelClose={() => setVisibleModal(false)} onSubmit={onSubmitUploadDuc} />

        </ProfileComponentStyled>
    );
});

export default ProfileComponent;
