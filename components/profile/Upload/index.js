import React from 'react';
import UploadProfileComponentStyled, { UploadSection } from './styled';
import Image from 'next/image';
import { FaPlusCircle } from 'react-icons/fa';

function UploadProfile({ data, onChange, uploadData }) {
    return (
        <UploadProfileComponentStyled>
            <UploadSection>
                {data.avatar ? (
                    <>
                        <Image className="rounded-full border-2 z-0 p-3 h-[130px] object-cover" src={uploadData !== "" ? uploadData : data.avatar} width={130} height={130} alt="image" />
                        <label htmlFor="image">
                            <FaPlusCircle className="w-6 absolute bottom-0 right-0 z-50 cursor-pointer" size={50} />
                            <input type="file" name="file" id="image" className="hidden" onChange={(e) => onChange(e)} />
                        </label>
                    </>
                ) : null}
            </UploadSection>
        </UploadProfileComponentStyled>
    );
}

export default UploadProfile;
