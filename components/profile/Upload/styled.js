import styled from 'styled-components';

const UploadProfileComponentStyled = styled.div`
    display: flex;
    justify-content: center;
`;

export default UploadProfileComponentStyled;

const UploadSection = styled.div`
    position: relative;
`;

export { UploadSection };
