import styled from 'styled-components';

const ProfileComponentStyled = styled.div``;

export default ProfileComponentStyled;

const CardProfile = styled.div`
    min-height: 200px;
    padding: 1rem 1.2rem;
    background: #fff;
    margin-top: 1rem;
    margin-left: 2rem;
    margin-right: 2rem;
    border-radius: 15px;

    :last-child {
        margin-bottom: 1rem;
    }
`;

const CardProfileRow = styled.div`
    display: flex;
    justify-content: space-between;
    flex-direction: row;
`;

export { CardProfile, CardProfileRow };
