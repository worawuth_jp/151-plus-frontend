import React from 'react';
import Btn from '../shared/Button';
import Input from '../shared/Input';
import ChangePasswordComponentStyled from './styled';

function ChangePasswordComponent() {
    return (
        <ChangePasswordComponentStyled>
            <div>
                <div className="flex">
                    <div className="card-change-password">
                        <div className="card-content">
                            <div className="text-sm text-[#666666] underline">เปลี่ยนรหัสผ่าน</div>

                            <div className="mt-3">
                                <Input label={'เลขบัตรประชาชน'} placeholder="กรอกเลข 13 หลัก ไม่ต้องใส่ - กั้น" />
                            </div>

                            <div className="">
                                <Input type="password" label={'รหัสผ่านใหม่'} placeholder="กรอกรหัสผ่านใหม่" />
                            </div>
                            
                            <div className="">
                                <Input type="password" label={'ยืนยันรหัสผ่านใหม่อีกครั้ง'} placeholder="รหัสผ่านทั้ง 2 ช่องต้องตรงกัน" />
                            </div>

                            <div>
                                <Btn fullWidth>ยืนยันการเปลี่ยนรหัสผ่าน</Btn>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ChangePasswordComponentStyled>
    );
}

export default ChangePasswordComponent;
