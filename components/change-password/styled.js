import styled from 'styled-components';

const ChangePasswordComponentStyled = styled.div`
    .card-change-password {
        min-height: 200px;
        min-width: 90%;
        background: #fff;
        margin-left: auto;
        margin-right: auto;
        border-radius: 15px;
        margin-top: 1rem;
        margin-bottom: 1rem;

        .card-content {
            padding: 1rem;
        }
    }
`;

export default ChangePasswordComponentStyled;
