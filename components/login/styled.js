import styled from "styled-components";

const LoginComponentStyle = styled.div`
height: 100vh;

.login-card{
    border-radius: 20px;
    min-width: 20rem;
    min-height: 10rem;

    .logo {
        width: 4.5rem;
    }
}

.login-footer{

}
`;

export default LoginComponentStyle;