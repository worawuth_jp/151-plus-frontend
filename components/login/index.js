import React, { useState } from 'react';
import { useRouter } from 'next/router';

import Btn from '../shared/Button';
import Input from '../shared/Input';
import LoginComponentStyle from './styled';

// react hhom
import { useForm } from 'react-hook-form';

// service
import authService from '../../services/auth.service';
import { ToastAlert } from '../shared/Toast';
import Loading from '../shared/Loading';

function LoginComponent(props) {
    const { register, handleSubmit } = useForm({
        mode: 'onChange',
    });
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setLoading] = useState(false);
    const router = useRouter();
    // const { ref, ...rest } = register('firstName');

    const onSubmit = async (data) => {
        try {
            setLoading(true);
            const payload = {
                username: username,
                password: password,
            };
            const response = await authService.login(payload);
            let { result, status } = response.data;
            if (status.code === 200 && status.message.includes('success')) {
                // set data to local storage
                await localStorage.setItem('access_token', JSON.stringify(result.access_token));
                await localStorage.setItem('data_member', JSON.stringify(result.member));
                ToastAlert({ text: 'เข้าสู่ระบบสำเร็จ' });
                setLoading(false);
                router.push('/');
            }
        } catch (err) {
            console.log(err);
            ToastAlert({ text: 'เข้าสู่ระบบล้มเหลว' });
        }
    };

    return (
        <LoginComponentStyle>
            <Loading isLoading={isLoading} />
            <div className=" container flex h-screen">
                <div className="login-card mx-auto bg-white my-auto p-3">
                    <img src="/images/logo.png" className="logo mx-auto" />
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <Input
                            className={'mt-[20px]'}
                            type="text"
                            label={'Phone or ID Card'}
                            required={true}
                            onChange={(e) => setUsername(e.target.value)}
                            name="username"
                            register={register}
                        />
                        <Input
                            label="Password"
                            type="password"
                            onChange={(e) => setPassword(e.target.value)}
                            name="password"
                            register={register}
                        />
                        <div className="login-footer">
                            <Btn primary variant="contained" fullWidth type="submit">
                                เข้าสู่ระบบ
                            </Btn>
                        </div>
                    </form>
                </div>
            </div>
        </LoginComponentStyle>
    );
}

export default LoginComponent;
