import React from 'react';
import LoginLayoutStyle from './styled';

function LoginLayout(props){
    return (
        <LoginLayoutStyle>
            <div className="min-h-screen max-h-screen max-w-sm ml-auto mr-auto max-w-screen-sm h-full bg-black overflow-auto ">{props.children}</div>
        </LoginLayoutStyle>
    );
};

export default LoginLayout;
