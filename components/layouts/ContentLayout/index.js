import React, { useEffect, useRef, useState } from 'react';
import ContentLayoutStyled, { AvatarProfile } from './styled';
import { FaBars } from 'react-icons/fa';
import Link from 'next/link';
import { useWindowSize } from 'react-use';
import classNames from 'classnames';
import { useRouter } from 'next/router';
import { HiOutlineShoppingCart } from "react-icons/hi";
import Image from 'next/image';
import { useSelector } from 'react-redux';
import { selectItems } from '../../../features/cart/cartSlice';

// redux
import StorageService from '../../../utils/StorageService';
import { store } from '../../../app/store';
import { hydrate } from '../../../features/cart/cartSlice';

// service
import memberService from '../../../services/member.service';

function ContentLayout(props){
    const navRef = useRef({});
    const { height } = useWindowSize();
    const [navHeight, setNavHeight] = useState(60);
    const [windowHeight, setWindowHeight] = useState(0);
    const [openMenu, setOpenMenu] = useState(false);
    const [role, setRoleCheck] = useState('');
    const router = useRouter();
    const items = useSelector(selectItems);

    useEffect(() => {
        setNavHeight(navRef.current?.offsetHeight);
        setWindowHeight(height);
    }, [height, navRef]);

    useEffect(() => {
        store.subscribe(() => {
            StorageService.set('cart', JSON.stringify(store.getState().cart));
        });
        let cart = StorageService.get('cart');
        cart = cart ? JSON.parse(cart) : { items: [] };
        store.dispatch(hydrate(cart));

        // get data info
        getUserDataInfo();
    }, []);

    async function getUserDataInfo() {
        try {
            let dataMember = await localStorage.getItem('data_member');
            let dataParse = JSON.parse(dataMember);
            const response = await memberService.getUserInfo(dataParse.member_id);
            let { result, status } = response.data;
            if (status.code === 200 && status.message.includes('success')) {
                setRoleCheck(result[0].member_type);
            }
        } catch (err) {
            console.log(err);
        }
    }

    return (
        <ContentLayoutStyled height={windowHeight} navHeight={navHeight}>
            <div className="min-h-screen max-h-screen relative  ml-auto mr-auto max-w-screen-sm h-full bg-stone-300 overflow-hidden">
                <nav ref={navRef} className="h-16 absolute w-full bg-black flex justify-between pl-2 pr-2">
                    <div className="nav-section mr-3">
                        <div
                            className="nav-item"
                            onClick={() => {
                                setOpenMenu((prev) => !prev);
                            }}>
                            <FaBars size={30} />
                        </div>
                    </div>
                    <div className="nav-section mr-3">
                        <div className="nav-logo-background">
                            <img src="/images/logo.png" className="nav-logo" />
                        </div>
                    </div>

                    <div className="nav-section">
                        <div className="nav-item relative" onClick={() => router.push('/cart')}>
                            <HiOutlineShoppingCart className="w-8 link text-white mr-5" fontSize={'25px'} />
                            <div className="absolute -top-2 right-4 rounded-full text-white bg-red-500 p-1 flex items-center justify-center text-xs font-extrabold">
                                {items?.length}
                            </div>
                        </div>

                        <div className="nav-item" onClick={() => router.push('/profile')}>
                            <div className="nav-user">
                                <Image src="/images/profile/me.jpg" className="rounded-full" alt="profile" width={42} height={42}></Image>
                            </div>
                        </div>
                    </div>
                </nav>

                <div className="pt-[4rem] ">
                    {openMenu && (
                        <div className="nav-menu w-full absolute top-30 z-50">
                            <div className="nav-item-bar p-2">
                                <Link href={'/'}>
                                    <div>หน้าแรก</div>
                                </Link>
                            </div>

                            <div className="nav-item-bar p-2">
                                <Link href={'/products'}>
                                    <div>รายการสินค้า</div>
                                </Link>
                            </div>

                            <div className="nav-item-bar p-2">
                                <Link href={'/member/1'}>
                                    <div>สมาชิก</div>
                                </Link>
                            </div>

                            <div className="nav-item-bar p-2">
                                <Link href={'/order/all'}>
                                    <div>รายการคำสั่งซื้อของฉัน</div>
                                </Link>
                            </div>

                            <div className="nav-item-bar p-2">
                                <Link href={'/income-detail'}>
                                    <div>รายละเอียดรายได้</div>
                                </Link>
                            </div>

                            <div className="nav-item-bar p-2">
                                <Link href={'/change-password'}>เปลี่ยนรหัสผ่าน</Link>
                            </div>
                            {/* {['ADMIN'].includes(role) && ( */}
                            {['MEMBER'].includes(role) && (
                                <>
                                    {/* สรุปยอดขายในระบบ */}
                                    <div className="nav-item-bar p-2">
                                        <Link href={'/admin/report-management'}>
                                            <div onClick={() => router.push('/admin/report-management')}>Report Management</div>
                                        </Link>
                                    </div>

                                    <div className="nav-item-bar p-2">
                                        <Link href={'/admin/member-management'}>
                                            <div onClick={() => router.push('/admin/member-management')}>Member Management</div>
                                        </Link>
                                    </div>

                                    <div className="nav-item-bar p-2">
                                        <Link href={'/admin/order-management'}>
                                            <div onClick={() => router.push('/admin/order-management')}>Order Management</div>
                                        </Link>
                                    </div>

                                    <div className="nav-item-bar p-2">
                                        <Link href={'/admin/product-management'}>
                                            <div onClick={() => router.push('/admin/product-management')}>Prodct Management</div>
                                        </Link>
                                    </div>

                                    {/* คำร้องถอนเงิน */}
                                    <div className="nav-item-bar p-2">
                                        <Link href={'/admin/transaction-management'}>
                                            <div onClick={() => router.push('/admin/transaction-management')}>Transaction Management</div>
                                        </Link>
                                    </div>
                                </>
                            )}

                            <div className="nav-item-bar p-2 pb-3">
                                <Link href={'/logout'}>ออกจากระบบ</Link>
                            </div>
                        </div>
                    )}
                    <div className={classNames('px-2 h-full overflow-auto content-wrapper relative')}>{props.children}</div>
                </div>
            </div>
        </ContentLayoutStyled>
    );
};
export default ContentLayout;
