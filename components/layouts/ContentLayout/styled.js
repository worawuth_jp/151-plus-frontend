import styled from 'styled-components';

const ContentLayoutStyled = styled.main`
    user-select: none;
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;

    /* width */

    .content-wrapper {
        max-height: ${(props) => props.height - props.navHeight}px;
        ::-webkit-scrollbar {
            width: 10px;
            
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: rgba(70, 70, 70, 0.5);
            border-radius: 10px !important;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }
    }

    .nav-section {
        display: flex;
        color: #fff;
        align-items: center;
    }

    .nav-logo {
        width: 3.5rem;
    }

    .nav-logo-background {
        background: #fff;
        padding: 0.221rem;
        padding-left: 1.5rem;
        padding-right: 1.5rem;
        cursor: pointer;
    }

    .nav-item-bar:hover {
        cursor: pointer;
        background: #535353;
    }

    .nav-item:hover {
        cursor: pointer;
    }

    .nav-menu {
        background: #383838;
        border: solid #434242 thin;
        color: #fff;
    }

    .nav-user {
        border-radius: 50%;
        min-width: 42px;
        min-height: 42px;
        background: #535353;
        max-width: 42px;
        max-height: 42px;
    }
`;

export default ContentLayoutStyled;

const AvatarProfile = styled.div`
    border-radius: 15px;
`;

export { AvatarProfile };