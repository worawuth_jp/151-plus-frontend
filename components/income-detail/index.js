import React, { forwardRef } from 'react';
import IncomeDetail from './IncomeDetail';
import HistoryComponentStyled, { CardOrder } from './styled';

// data picker
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

const IncomeDetailComponent = forwardRef((props, ref) => {
    return (
        <HistoryComponentStyled>
            <div className="flex justify-between">
                <div className="text-black font-semibold text-md m-3 ml-10 mt-5">สรุปการรับสิทธิประโยชน์</div>
            </div>

            <CardOrder>
                <div className="mt-2 flex justify-end">
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DatePicker />
                    </LocalizationProvider>
                </div>
                <IncomeDetail />
            </CardOrder>
        </HistoryComponentStyled>
    );
});

export default IncomeDetailComponent;
