import numeral from 'numeral';
import React from 'react';
import IncomeDetailStyled from './styled';

// mui
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineDot from '@mui/lab/TimelineDot';
import moment from 'moment';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import TimelineOppositeContent, { timelineOppositeContentClasses } from '@mui/lab/TimelineOppositeContent';

const LIST = [
    {
        listId: 1,
        date: '2023-04-12T08:53:40.616Z',
        title: 'รายการขายสินค้า',
        price: 550,
        type: 'plus',
        items: [
            {
                name: 'Join Plus',
                des: 'โอนเงินจาก 12312312321 ไปยัง 423423423423',
            },
        ],
    },

    {
        listId: 2,
        date: '2023-03-12T08:53:40.616Z',
        title: 'Commissiom',
        price: 1550,
        type: 'plus',
        items: [
            {
                name: 'Join Plus',
                des: 'โอนเงินจาก 12312312321 ไปยัง 423423423423',
            },
            {
                name: 'ฟ้าละลายโจร',
                des: 'โอนเงินจาก 12312312321 ไปยัง 423423423423',
            },
        ],
    },

    {
        listId: 3,
        date: '2023-04-14T08:53:40.616Z',
        title: 'Commissiom',
        price: 1550,
        type: 'plus',
        items: [
            {
                name: 'Join Plus',
                des: 'โอนเงินจาก 12312312321 ไปยัง 423423423423',
            },
        ],
    },
];

function IncomeDetail() {
    return (
        <IncomeDetailStyled>
            <div className="card-details">
                <div>
                    <div className="mt-3 border-none">
                        {LIST.map((item, index) => (
                            <Timeline
                                key={index}
                                sx={{
                                    [`& .${timelineOppositeContentClasses.root}`]: {
                                        flex: 0.4,
                                    },
                                }}>
                                <Accordion elevation={0}>
                                    <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                                        {/* item */}
                                        <TimelineItem>
                                            <TimelineOppositeContent color="textSecondary">
                                                <div>{moment(item.date).format('llll')}</div>
                                            </TimelineOppositeContent>
                                            <TimelineSeparator>
                                                <TimelineDot />
                                                <TimelineConnector />
                                            </TimelineSeparator>
                                            <TimelineContent>
                                                <div className="flex justify-between">
                                                    <div>{item.title}</div>
                                                    <div className="flex justify-end text-[#008000] mr-2">{numeral(item.price).format(',.##')} </div>
                                                </div>
                                            </TimelineContent>
                                        </TimelineItem>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <div className="flex flex-col mx-10">
                                            <div className="text-[#6a6a6a] mb-4">รายการ</div>
                                            <div>
                                                <ul>
                                                    <li className="flex flex-col">
                                                        <spann>ซื้อสินค้า Join Plus</spann>
                                                        <spann>โอนเงินจาก 12312312321 ไปยัง 423423423423</spann>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </AccordionDetails>
                                </Accordion>
                            </Timeline>
                        ))}
                    </div>
                </div>
            </div>
            <div className="text-[#008000] text-right mx-10 font-bold">ยอดรวมทั้งหมด : 2,000.00 บาท</div>
        </IncomeDetailStyled>
    );
}

export default IncomeDetail;
