import styled from 'styled-components';

const InputStyled = styled.div`
    /* width: 100%;
    border: #C6C6C6 solid 1px;
    border-radius: 7px;
    padding: 5px;

    :active :hover {
        outline: #979797 !important;
        border: #979797 !important;
    } */

    padding: 5px 5px;

    fieldset {
        border-radius: 10px;
    }
`;

export default InputStyled;
