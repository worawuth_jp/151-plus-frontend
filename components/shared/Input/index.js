import { TextField } from '@mui/material';
import React from 'react';
import InputStyled from './styled';

function Input({
    name,
    register = () => {
        return {};
    },
    className,
    value,
    size = 'small',
    label,
    variant = 'outlined',
    error,
    fullWidth = true,
    required,
    ...props
}) {
    return (
        <InputStyled className={className}>
            <TextField
                {...register(name)}
                size={size}
                value={value}
                label={label}
                variant={variant}
                error={error}
                fullWidth={fullWidth}
                {...props}
                required={required}
            />
        </InputStyled>
    );
}

export default Input;
