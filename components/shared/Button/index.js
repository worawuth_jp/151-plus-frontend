import { Button } from '@mui/material';
import classNames from 'classnames';
import React from 'react';
import ButtonStyled from './styled';

function Btn({ className, buttonClassName, children, variant = 'contained', black, quaternary, tertiary, primary, secondary, color, ...props }) {
    const generateColor = () => {
        let result = '';
        if (primary) {
            result = 'bg-[#DA3D3D] hover:bg-[#C71919]';
        }

        if (secondary) {
            result = 'bg-[#0C66C0] hover:bg-[#0D4D8E]';
        }

        if (tertiary) {
            result = 'bg-[#EB9110] hover:bg-[#D5820C]';
        }

        if (quaternary) {
            result = 'bg-[#039A3A] hover:bg-[#24673C]';
        }

        if (black) {
            result = 'bg-[#000] hover:bg-[#333333]';
        }

        return result;
    };

    return (
        <ButtonStyled className={className}>
            <Button className={classNames(buttonClassName, generateColor())} variant={variant} color={color} {...props}>
                {children}
            </Button>
        </ButtonStyled>
    );
}

export default Btn;
