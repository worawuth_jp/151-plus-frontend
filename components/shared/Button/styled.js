import styled from 'styled-components';

const ButtonStyled = styled.div`
    padding: 5px 5px;

    button {
        border-radius: 10px;
    }

    & > button,
    [type='button'],
    [type='reset'],
    [type='submit'] {
        -webkit-appearance: button;
        background-color: none !important;
        background-image: none;
    }
`;

export default ButtonStyled;
