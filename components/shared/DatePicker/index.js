import * as React from 'react';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import InputStyled from './styled';

const style = {
    width: '100%'
};

export default function DatePickerMain() {
    return (
        <InputStyled>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DatePicker sx={style} />
            </LocalizationProvider>
        </InputStyled>
    );
}
