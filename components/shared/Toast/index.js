import React from 'react';
import { toast } from 'react-toastify';

export function ToastAlert({
    position = 'top-right',
    autoClose = 4000,
    theme = '',
    style = {
        background: 'white',
        color: '#1f2937',
        fontFamily: 'Poppins, sans-serif',
        height: 'auto',
    },
    text = '',
    hideProgressBar = false,
    pauseOnHover = false,
    draggable = true,
    draggablePercent = 25,
}) {
    const getStyle = () => {
        switch (theme) {
            case 'primary':
                return {
                    background: 'white',
                    color: '#1f2937',
                    fontFamily: 'Poppins, sans-serif',
                    height: 'auto',
                };
            case 'warning':
                return {
                    background: 'red',
                    color: '#fff',
                    fontFamily: 'Poppins, sans-serif',
                    height: 'auto',
                };
            default:
                return { ...style };
        }
    };

    return toast(text, {
        position: position,
        autoClose: autoClose,
        style: getStyle(),
        hideProgressBar: hideProgressBar,
        pauseOnHover: pauseOnHover,
        draggable: draggable,
        draggablePercent: draggablePercent,
    });
}

export default function Toast({}) {
    return <Toast />;
}
