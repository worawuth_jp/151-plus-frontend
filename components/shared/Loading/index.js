import React from 'react';
import LoadingStyled from './styled';
import { CircularProgress, Dialog, DialogContent } from '@mui/material';

function Loading({ isLoading = true }) {
    return (
        <Dialog open={isLoading}>
            <DialogContent>
                <LoadingStyled>{isLoading && <CircularProgress />}</LoadingStyled>
            </DialogContent>
        </Dialog>
    );
}

export default Loading;
