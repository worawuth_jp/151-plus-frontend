import styled from 'styled-components';

const ProductStyled = styled.div`
    background: ${(props) => props.bg};
    //background: linear-gradient(180deg, rgba(29, 20, 1, 1) 12%, rgb(236, 53, 53, 1) 98%);

    min-height: 50px;
    border-radius: 10px;
    color: #fff;
    padding: 1rem;
    cursor: pointer;
`;

export default ProductStyled;
