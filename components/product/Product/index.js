import React from 'react';
import ProductStyled from './styled';

// redux
import { addToCart } from '../../../features/cart/cartSlice';
import { useDispatch } from 'react-redux';

// import { ShoppingCartIcon } from '@heroicons/react/solid';
import { FaShoppingBasket } from 'react-icons/fa';
import Image from 'next/image';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Currency from 'react-currency-formatter';
import Fade from 'react-reveal/Fade';
import { HiOutlineShoppingCart } from 'react-icons/hi';

function ProductCard({ _id, index, title, price, description, category, image }) {
    const dispatch = useDispatch();
    const router = useRouter();
    // const { _id, title, price, description, category, thumbnail} = data;

    const addItemToCart = () => {
        dispatch(
            addToCart({
                _id,
                title,
                price,
                description,
                category,
                image,
                qty: 1,
                toast: true,
            })
        );
    };

    return (
        <ProductStyled>
            <Fade bottom>
                <div className="relative flex flex-col  bg-white z-20  md:p-8 p-6 rounded-md shadow-xl">
                    <Image
                        src={image}
                        height={160}
                        width={200}
                        alt=""
                        className="cursor-pointer h-[160px]"
                        onClick={() => router.push(`/product-details/${_id}`)}
                    />
                    <h4 className="my-3 link font-medium capitalize text-black">
                        <Link href={`/product-details/${_id}`}>{title}</Link>
                    </h4>
                    <p className="text-xs  mb-2 line-clamp-2 text-gray-500 link">
                        <Link href={`/product-details/${_id}`}>{description}</Link>
                    </p>
                    <div className="mb-5 mt-2 font-bold text-gray-700">
                        <Currency quantity={price} currency="THB" />
                    </div>
                    <button
                        className="mt-auto button flex items-center justify-center shadow-md bg-black rounded-lg p-2"
                        onClick={addItemToCart}>
                        <HiOutlineShoppingCart className="w-4 " fontSize={'25px'} />
                        <span className="ml-2 ">Add to Cart</span>
                    </button>
                </div>
            </Fade>
        </ProductStyled>
    );
}

export default ProductCard;
