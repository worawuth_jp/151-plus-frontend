import styled from 'styled-components';

const ProductComponentStyled = styled.div``;

export default ProductComponentStyled;

const CardProduct = styled.div`
    min-height: 200px;
    padding: 1rem 1.2rem;
    background: #fff;
    margin-top: 1rem;
    margin-left: 2rem;
    margin-right: 2rem;
    border-radius: 15px;

    :last-child {
        margin-bottom: 1rem;
    }
`;

export { CardProduct };
