import React, { forwardRef } from 'react';
import Product from './Product';
import ProductComponentStyled, { CardProduct } from './styled';
import { FaShoppingBasket } from "react-icons/fa";

// mock data products
const data = [
    {
        id: 1,
        price: 1500,
        title: 'Join Plus',
        description: 'An apple mobile which is nothing like apple',
        discountPercentage: 0,
        stock: 200,
        category: 'food supplement',
        brand: 'Join Plus',
        thumbnail: '/images/products/joinplus.jpg',
        createdAt: '2022-01-21T15:21:00Z',
    },
    {
        id: 2,
        price: 1390,
        title: 'CHL',
        description: 'An apple mobile which is nothing like apple',
        discountPercentage: 0,
        stock: 200,
        category: 'food supplement',
        brand: 'CHL',
        thumbnail: '/images/products/chl.jpg',
        createdAt: '2022-01-21T15:21:00Z',
    },
    {
        id: 3,
        price: 1500,
        title: 'Z-TA',
        description: 'An apple mobile which is nothing like apple',
        discountPercentage: 0,
        stock: 200,
        category: 'food supplement',
        brand: 'Z-TA',
        thumbnail: '/images/products/z-ta.jpg',
        createdAt: '2022-01-21T15:21:00Z',
    },
    {
        id: 4,
        price: 1890,
        title: 'nakarat',
        description: 'An apple mobile which is nothing like apple',
        discountPercentage: 0,
        stock: 200,
        category: 'food supplement',
        brand: 'nakarat',
        thumbnail: '/images/products/nakarat.jpg',
        createdAt: '2022-01-21T15:21:00Z',
    },
    {
        id: 5,
        price: 1500,
        title: 'Promix',
        description: 'An apple mobile which is nothing like apple',
        discountPercentage: 0,
        stock: 200,
        category: 'food supplement',
        brand: 'Join Plus',
        thumbnail: '/images/products/i-herb_prommix.jpg',
        createdAt: '2022-01-21T15:21:00Z',
    },
    {
        id: 6,
        price: 1100,
        title: 'ASX',
        description: 'An apple mobile which is nothing like apple',
        discountPercentage: 0,
        stock: 200,
        category: 'food supplement',
        brand: 'Join Plus',
        thumbnail: '/images/products/asx.jpg',
        createdAt: '2022-01-21T15:21:00Z',
    },
    {
        id: 7,
        price: 890,
        title: 'Q-Gazzy',
        description: 'An apple mobile which is nothing like apple',
        discountPercentage: 0,
        stock: 200,
        category: 'food supplement',
        brand: 'Join Plus',
        thumbnail: '/images/products/qgazzy.jpg',
        createdAt: '2022-01-21T15:21:00Z',
    },
    {
        id: 8,
        price: 350,
        title: 'Monice',
        description: 'An apple mobile which is nothing like apple',
        discountPercentage: 0,
        stock: 200,
        category: 'food supplement',
        brand: 'Join Plus',
        thumbnail: '/images/products/monice.jpg',
        createdAt: '2022-01-21T15:21:00Z',
    },
    {
        id: 9,
        price: 320,
        title: 'ฟ้าละลายโจร',
        description: 'An apple mobile which is nothing like apple',
        discountPercentage: 0,
        stock: 200,
        category: 'food supplement',
        brand: 'ฟ้าละลายโจร',
        thumbnail: '/images/products/andrographis.jpg',
        createdAt: '2022-01-21T15:21:00Z',
    },
    {
        id: 10,
        price: 340,
        title: 'Thai Herb',
        description: 'An apple mobile which is nothing like apple',
        discountPercentage: 0,
        stock: 200,
        category: 'food supplement',
        brand: 'thai herbs',
        thumbnail: '/images/products/thaiherbs.jpg',
        createdAt: '2022-01-21T15:21:00Z',
    },
];

const ProductComponent = forwardRef((props, ref) => {
    return (
        <ProductComponentStyled>
            <CardProduct>
                <div className="underline font-bold">สินค้าทั้งหมด</div>
                <div className="grid grid-flow-row-dense sm:grid-cols-2 grid-cols-1 mx-auto max-w-screen-xl gap-x-6 gap-y-8">
                    {data.map((item, index) => (
                        <Product
                            // key={`${item.id}${index}`}
                            index={index + 1}
                            key={`product-${item.id}`}
                            _id={item.id}
                            title={item.title}
                            price={item.price}
                            description={item.description}
                            category={item.category}
                            image={item.thumbnail}
                        />
                    ))}
                </div>

                {data.length === 0 && (
                    <div className="text-xl font-bold text-gray-400 my-auto h-[170px] text-center flex justify-center">
                        <div className="self-center">
                            <div className="justify-center">
                                ไม่มีสินค้า <FaShoppingBasket className="mx-auto" size={50} />
                            </div>
                        </div>
                    </div>
                )}
            </CardProduct>
        </ProductComponentStyled>
    );
});

export default ProductComponent;
