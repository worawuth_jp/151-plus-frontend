import React, { forwardRef } from 'react';
import CheckoutProduct from './Checkout';
import CheckoutComponentStyled, { CardCart } from './styled';

const CheckoutComponent = forwardRef((props, ref) => {
    return (
        <CheckoutComponentStyled>
            <CardCart>
                <div className="underline font-bold">รายละเอียดการชำระเงิน</div>
                <CheckoutProduct />
            </CardCart>
        </CheckoutComponentStyled>
    );
});

export default CheckoutComponent;
