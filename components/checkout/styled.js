import styled from 'styled-components';

const CheckoutComponentStyled = styled.div``;

export default CheckoutComponentStyled;

const CardCart = styled.div`
    min-height: 200px;
    padding: 1rem 1.2rem;
    background: #fff;
    margin-top: 1rem;
    margin-left: 2rem;
    margin-right: 2rem;
    border-radius: 15px;
`;

const CardCheckout = styled.div`
    display: flex;
    justify-content: center;
    margin: 20px;
`;

export { CardCart, CardCheckout };
