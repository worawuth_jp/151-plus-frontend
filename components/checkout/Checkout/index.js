import Image from 'next/image';

function CheckoutProduct() {
    return (
        <div className="mt-2 flex justify-start flex-col">
            <div>
                <div className="font-bold text-black mt-4">ไอดีไลน์</div>

                <div>
                    <div className="flex flex-row w-full">
                        <a href="http://line.me/ti/p/~@151PLUS" className='text-blue-500'>@151PLUS</a>
                        <span className="ml-2">(แอดไลน์เพื่อติดต่อแอดมิน)</span>
                    </div>
                </div>
            </div>

            <div>
                <div className="font-bold text-black mt-4">Scan this:</div>

                <div className="flex justify-center">
                    <Image src="/images/line/line.png" alt="line" width={200} height={200} />
                </div>
            </div>

            <div>
                <div className="font-bold text-black mt-4">บัญชีธนาคาร</div>
                <div>
                    <img className="w-10 m-2" src="https://f.ptcdn.info/801/022/000/1409170288-b60f8c1e0e-o.png" />
                    ธนาคารกรุงเทพ จำกัด (มหาชน)
                </div>

                <div>
                    <div className="flex flex-row w-full">
                        <span>เลขที่บัญชี : </span>
                        <span className="ml-2">1112540584</span>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CheckoutProduct;
