import React, { forwardRef } from 'react';
import Order from './Order';
import OrderComponentStyled, { CardOrder } from './styled';
import { FaShoppingBasket } from 'react-icons/fa';
import { useRouter } from 'next/router';

const data = [
    {
        orderId: 1,
        orderNo: '20220121-A0001-000001',
        total: 2000.0,
        deliveryFee: 100,
        address: '1/2 กกกกก',
        member: {
            phoneNumber: '092515555555',
            firstName: 'นายก',
            lastName: 'นามสกุลข',
        },
        payment: 'TRANSFER',
        paymentStatus: 'SUCCESS',
        orderStatus: 'INPROGRESS',
        createdAt: '2022-01-21T15:21:00Z',
    },
    {
        orderId: 2,
        orderNo: '20220111-A0001-000002',
        total: 1750.0,
        deliveryFee: 100,
        address: '1/2 กกกกก',
        member: {
            phoneNumber: '092515555555',
            firstName: 'นายก',
            lastName: 'นามสกุลข',
        },
        payment: 'TRANSFER',
        paymentStatus: 'PENDING',
        orderStatus: 'INPROGRESS',
        createdAt: '2022-01-11T10:11:00Z',
    },

    {
        orderId: 3,
        orderNo: '20220111-A0001-000003',
        total: 3850.0,
        deliveryFee: 100,
        address: '1/2 กกกกก',
        member: {
            phoneNumber: '092515555555',
            firstName: 'นายก',
            lastName: 'นามสกุลข',
        },
        payment: 'TRANSFER',
        paymentStatus: 'NOT_PAID',
        orderStatus: 'INPROGRESS',
        createdAt: '2022-01-11T10:11:00Z',
    },
];

const OrderComponent = forwardRef((props, ref) => {
    const router = useRouter();
    const routerOrderDetail = (id) => {
        router.push(`${id}`);
    };

    return (
        <OrderComponentStyled>
            <CardOrder>
                <div className="underline font-bold">คำสั่งซื้อ</div>
                {data.map((item, index) => (
                    <Order key={`${item.orderId}${index}`} index={index + 1} data={item} onClick={() => routerOrderDetail(item?.orderId)} />
                ))}

                {data.length === 0 && (
                    <div className="text-xl font-bold text-gray-400 my-auto h-[170px] text-center flex justify-center">
                        <div className="self-center">
                            <div className="justify-center">
                                ไม่มีคำสั่งซื้อ <FaShoppingBasket className="mx-auto" size={50} />
                            </div>
                        </div>
                    </div>
                )}
            </CardOrder>
        </OrderComponentStyled>
    );
});

export default OrderComponent;
