import styled from 'styled-components';

const OrderStyled = styled.div`
    background: ${(props) => props.bg};
    // min-height: 50px;
    border-radius: 10px;
    color: #fff;
    padding: 1rem;
    cursor: pointer;

`;

export default OrderStyled;
