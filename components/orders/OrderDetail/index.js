import React from 'react';
import OrderStyled from './styled';
import { useState } from 'react';
import moment from 'moment';
import Currency from 'react-currency-formatter';
import Fade from 'react-reveal/Fade';
import Link from 'next/link';
import { MinusSmIcon, PlusIcon } from '@heroicons/react/solid';
import Image from 'next/image';
import { useRouter } from 'next/router';

function OrderDetail() {
    const router = useRouter();
    const orderId = router.query.orderId;
    const [order, setOrder] = useState({
        orderId: 2,
        timestamp: '2023-04-12T08:53:40.616Z',
        order_status: {
            current: {
                status: 'delivered',
            },
            info: [
                {
                    status: 'delivered',
                    timestamp: '2023-04-12T08:53:40.616Z',
                },
            ],
        },
        items: [],
    });

    return (
        <OrderStyled>
            <div className="border bg-white rounded-md max-w-screen-xl  shadow-sm">
                <div className="flex p-5 bg-gray-100 text-sm text-gray-700">
                    <div>
                        <p className="sm:text-2xl text-xl font-semibold mb-2">Order Details</p>
                        <p className="xs:text-sm text-xs text-black ">{order ? moment(order?.timestamp).format('llll') : <Skeleton />}</p>
                        <div className="text-sm">Tracking Number: 20220121-A0001-000001</div>
                        <div className="font-bold">นายก นามสกุลข</div>
                    </div>
                    {order && <p className="lg:text-xl text-black  md:text-lg text-base font-medium whitespace-nowrap  self-end flex-1 text-right text-blue-500">1 items</p>}
                </div>
                <div className="p-5 md:p-10 sm:p-8">
                    <select
                        className="shadow text-black leading-tight focus:outline-none focus:shadow-outline border xs:text-sm text-xs p-2 rounded bg-blue-500 text-black capitalize"
                        value={order?.order_status?.current?.status}>
                        {/* <option value="shipping soon">Shipping soon</option>
                        <option value="shipped">Shipped</option>
                        <option value="out for delivery">Out for delivery</option> */}
                        <option value="delivered">Delivered</option>
                    </select>

                    <div className="space-y-6 lg:text-lg sm:text-base text-sm">
                        <div
                            className={`my-2 text-black  p-4 border-2 ${
                                order?.order_status?.current?.status === 'cancelled'
                                    ? 'text-red-500 text-black  border-red-500 bg-red-100'
                                    : order?.order_status?.current.status === 'delivered'
                                    ? 'text-green-500  text-black  border-green-500 bg-green-100'
                                    : 'text-blue-500 text-black  border-blue-500 bg-blue-100'
                            }  rounded-md`}>
                            <h1 className="font-semibold mb-4">Order Status</h1>
                            <ul className="space-y-2">
                                {order?.order_status?.info?.map(({ status, timestamp }, i) => (
                                    <li className="flex sm:items-center sm:justify-between sm:flex-row flex-col" key={`orderStatus-${i}`}>
                                        <span className="sm:text-sm text-black  text-xs font-medium capitalize">{status}</span>
                                        <span className="sm:text-sm  text-black  text-xs">{moment(timestamp).format('llll')}</span>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        {order?.order_status?.current?.status === 'cancelled' ? (
                            <p className="my-2 text-xs xs:text-sm text-black  text-red-500">* Money will be refunded within 24 hour</p>
                        ) : (
                            <></>
                        )}

                        <p className="whitespace-nowrap font-semibold overflow-x-auto text-black  hideScrollBar">
                            ORDER ID -<span className="text-green-500 text-black font-medium ml-2">{orderId}</span>
                        </p>
                        <p className="font-semibold text-black  whitespace-nowrap overflow-x-auto hideScrollBar flex items-center">
                            EMAIL -<span className="text-sm font-normal text-black  ml-2">admin@@gmail.com</span>
                        </p>
                        <div>
                            <h3 className="font-semibold mb-2 text-black  uppercase">Address </h3>
                            <div className="text-sm text-gray-700">
                                <p>
                                    <span className="font-semibold"> Name - </span>
                                    {/* {order?.shipping?.name} */}ก ไก่
                                </p>
                                <p>
                                    <span className="font-semibold">City - </span>
                                    {/* {order?.shipping?.address?.city} */}
                                    หาดใหญ่
                                </p>
                                <p>
                                    <span className="font-semibold">Country - </span>
                                    {/* {order?.shipping?.address?.country} */}
                                    ไทย
                                </p>
                                <p>
                                    <span className="font-semibold">Line 1 - </span>
                                    {/* {order?.shipping?.address?.line1}, */}
                                </p>
                                <p>
                                    <span className="font-semibold">Line 2 - </span>
                                    {/* {order?.shipping?.address?.line2} */}
                                    admin23!
                                </p>
                                <p>
                                    <span className="font-semibold">Postal Code - </span>
                                    {/* {order?.shipping?.address?.postal_code} */}
                                    10240
                                </p>
                                <p>
                                    <span className="font-semibold">State - </span>
                                    {/* {order?.shipping?.address?.state} */}
                                    กทม.
                                </p>
                            </div>
                        </div>
                        <div>
                            <h3 className="font-semibold mb-2 text-black uppercase">Amount</h3>
                            <div className="text-sm text-gray-700">
                                <p className="text-xl">
                                    <span className="font-semibold">Subtotal - </span>
                                    <Currency quantity={30000 / 100} currency="INR" />
                                </p>
                                <p className="text-green-500 text-xl font-semibold">
                                    <span>Shipping - </span>
                                    <Currency quantity={31000 / 100} currency="INR" />
                                </p>
                                <p className="font-bold text-xl text-red-500">
                                    <span className="font-semibold">Total - </span>
                                    <Currency quantity={30000 / 100} currency="INR" />
                                </p>
                            </div>
                        </div>
                        <div>
                            <h4 className="font-semibold mb-2 text-black uppercase">Items</h4>
                            <Fade bottom>
                                <div className={`block mt-5 py-6 text-black sm:grid sm:grid-cols-5 border-b border-gray-300`}>
                                    <div className="text-center sm:text-left">
                                        <Image
                                            src={'/images/products/joinplus.jpg'}
                                            width={150}
                                            height={150}
                                            objectFit="contain"
                                            className="cursor-pointer"
                                            alt=""
                                            onClick={() => router.push(`/product-details/1`)}
                                        />
                                    </div>

                                    {/* M_iddle */}
                                    <div className="col-span-3 sm:p-4 mt-2 mb-6 sm:my-0">
                                        <h4 className="mb-3 text-black link lg:text-xl md:text-lg text-base capitalize font-medium">
                                            <Link href={`/product-details/1`}>Join Plus</Link>
                                        </h4>
                                        <p className="lg:text-sm text-xs my-2  mb-4 line-clamp-3 link text-gray-500">
                                            <Link href={`/product-details/1`}>'An apple mobile which is nothing like apple'</Link>
                                        </p>
                                        <span className="font-medium md:text-base text-sm">
                                            {2} × <Currency quantity={2222} currency="THB" /> =
                                            <span className="font-bold text-gray-700 mx-1">
                                                <Currency quantity={2000} currency="THB" />
                                            </span>
                                        </span>
                                    </div>

                                    {/* Buttons on the right of the products */}
                                    <div className="flex flex-col space-y-4 my-auto  justify-self-end">
                                        <div className="flex justify-between">
                                            <button className={`button sm:p-1 opacity-50`}>
                                                <MinusSmIcon className="h-5" />
                                            </button>
                                            <div className="p-2 whitespace-normal sm:p-1 sm:whitespace-nowrap">
                                                <span className="font-bold md:text-base text-sm text-gray-700">2</span>
                                            </div>
                                            <button className={`button sm:p-1 opacity-50`}>
                                                <PlusIcon className="h-5" />
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </Fade>
                        </div>
                        {order?.order_status?.current?.status && order?.order_status?.current?.status !== 'cancelled' && order?.order_status?.current?.status !== 'delivered' ? (
                            <div className="py-4">
                                <button className={`button-red py-2 px-12 capitalize w-full sm:text-base text-sm ${disabled ? 'opacity-50' : ''}`}>Cancel Order</button>
                            </div>
                        ) : (
                            <></>
                        )}
                    </div>
                </div>
            </div>
        </OrderStyled>
    );
}

export default OrderDetail;
