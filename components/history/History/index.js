import dayjs from 'dayjs';
import numeral from 'numeral';
import React from 'react';
import HistoryStyled from './styled';

function History({ data, index }) {
    const colorCard = () => {
        let result = '#004FB7';
        switch (data.paymentStatus) {
            case 'SUCCESS':
                result = '#007934';
                break;
            case 'PENDING':
                result = '#D58716';
                break;
            case 'NOT_PAID':
                result = '#5E5E5E';
                break;
            default:
                result = '#004FB7';
        }

        return result;
    };
    return (
        <HistoryStyled bg={colorCard()}>
            <div>
                <div className="flex flex-row justify-between">
                    <div>
                        <div className="flex">
                            <div className="text-[2rem] font-bold flex justify-start pr-3 align-center self-center">{index}</div>
                            <div>
                                <div className="text-xs text-[#FFE0B8] font-bold">{dayjs(data.createdAt).format('วันที่ DD-MM-YYYY เวลา HH:mm น.')}</div>
                                <div className="text-sm">{data.orderNo}</div>
                                <div className="font-bold">
                                    {data.member.firstName} {data.member.lastName}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="text-right">
                        <div className="font-bold">
                            {data.paymentStatus === 'SUCCESS'
                                ? 'ชำระเงินแล้ว'
                                : data.paymentStatus === 'PENDING'
                                ? 'รอยืนยัน'
                                : data.paymentStatus === 'NOT_PAID'
                                ? 'ยังไม่ได้ชำระ'
                                : ''}
                        </div>
                        <div className="text-xl">
                            <span className="text-sm">ราคาสินค้า </span>
                            <span className="font-bold">{numeral(data.total).format(',.##')}</span>
                        </div>
                        <div className="text-sm">ค่าส่ง {numeral(data.deliveryFee).format(',.##')}</div>
                    </div>
                </div>
            </div>
        </HistoryStyled>
    );
}

export default History;
