import classNames from 'classnames';
import React from 'react';
import { FaUserCircle } from 'react-icons/fa';
import { Member, Text } from '../styled';

function MemberItem({ name, memberId, active, disabled, onClick }) {
    return (
        <Member
            className={classNames('hover:bg-[#E0DEDE] rounded-[15px] p-2 cursor-pointer min-w-[7rem] max-w-[7rem]', { 'hover:bg-[#fff]': disabled, 'cursor-default': disabled })}
            onClick={() => {
                if (active && onClick()) {
                    onClick();
                }
            }}>
            <FaUserCircle className="icon mx-auto" size={52} />
            <Text className="text-black text-sm">{name}</Text>
            <Text className="text-sm">{memberId}</Text>
            {active ? <Text className="text-xs text-[#03730E] font-semibold">Active</Text> : <Text className="text-xs text-[#FD2A14] font-semibold">Inactive</Text>}
        </Member>
    );
}

export default MemberItem;
