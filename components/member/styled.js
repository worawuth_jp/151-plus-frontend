import styled from 'styled-components';

const MemberComponentStyled = styled.div `
    .card-member-details {
        border-radius: 15px;
        min-height: 300px;
        padding: 1rem 1.2rem;
        background: #fff;
        margin-top: 1.3rem;
        margin-left: 2rem;
        margin-right: 2rem;
    }

    .card-member-search {
        border-radius: 15px;
        min-height: 50px;
        padding: 1rem 1.2rem;
        background: #fff;
        margin-top: 1.3rem;
        margin-left: 2rem;
        margin-right: 2rem;
    }

    .card-member-create {
        border-radius: 15px;
        padding: 1rem 1.2rem;
        margin-left: 2rem;
        margin-right: 2rem;
        margin-bottom: -1rem;
    }
`;

export default MemberComponentStyled;

const MemberContainer = styled.div `
    display: flex;
    justify-content: center;

    padding: 0rem 0.43rem 0.8rem;
`;
const Member = styled.div `
    color: #807b7b;
    justify-content: center;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
    .icon {
        color: #000;
    }
`;

const Text = styled.div `
    text-align: center;
    padding-left: 10px;
    padding-right: 10px;
`;

export { MemberContainer, Member, Text };