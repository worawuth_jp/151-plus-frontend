import { Divider } from '@mui/material';
import React, { useState } from 'react';
import MemberComponentStyled, { MemberContainer } from './styled';
import MemberItem from './Item';
import { useRouter } from 'next/router';
// share components
import Input from '../shared/Input';
import Btn from '../shared/Button';
import Modal from '../shared/Modal';
import DatePickerMain from '../shared/DatePicker';

// react hook form
import { useForm, Controller } from 'react-hook-form';
import { useEffect } from 'react';

const USER_DATA = [
    {
        memberId: '1171',
        member_type: 'admin',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '11/10/66',
        member_status: 'Active',
        upline_id: '', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1172',
        member_type: 'user',
        first_name: 'ค ควาย',
        last_name: 'ออกลูก',
        password: 'test1234!',
        card_id: 1,
        birth_date: '11/10/66',
        member_status: 'Active',
        upline_id: '1171', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1173',
        member_type: 'user',
        first_name: 'จ จาน',
        last_name: 'ออกไข่',
        password: 'test1234!',
        card_id: 1,
        birth_date: '11/10/66',
        member_status: 'Active',
        upline_id: '1171', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1174',
        member_type: 'user',
        first_name: 'ม ม้า',
        last_name: 'ออกไข่',
        password: 'test1234!',
        card_id: 1,
        birth_date: '11/10/66',
        member_status: 'Active',
        upline_id: '1171', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1175',
        member_type: 'user',
        first_name: 'ข ไข่',
        last_name: 'ออกไข่',
        password: 'test1234!',
        card_id: 1,
        birth_date: '11/10/66',
        member_status: 'Active',
        upline_id: '1171', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1176',
        member_type: 'user',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '11/10/66',
        member_status: 'Active',
        upline_id: '1172', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1177',
        member_type: 'user',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '11/10/66',
        member_status: 'Active',
        upline_id: '1173', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1178',
        member_type: 'user',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '11/10/66',
        member_status: 'Active',
        upline_id: '1174', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1179',
        member_type: 'user',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '11/10/66',
        member_status: 'Active',
        upline_id: '1174', // ถ้าแอดมินไม่มี
        enable: true,
    },
    {
        memberId: '1180',
        member_type: 'user',
        first_name: 'ประไก่',
        last_name: 'รุ่งเจริญ',
        password: 'test1234!',
        card_id: 1,
        birth_date: '11/10/66',
        member_status: 'Active',
        upline_id: '1172', // ถ้าแอดมินไม่มี
        enable: true,
    },
];

function MemberComponent() {
    const [visible, setVisible] = useState(false);
    const [userData, setUserData] = useState(USER_DATA[0]);
    const router = useRouter();
    const memberID = router.query.id;
    const {
        register,
        control,
        handleSubmit,
        formState: { errors },
    } = useForm({
        defaultValues: {
            firstName: '',
            lastname: '',
            idCard: '',
            phone: '',
            dayOfBirth: '',
        },
    });

    useEffect(() => {
        if (memberID) {
            initialDataMember();
        }
    }, [memberID]);

    const initialDataMember = () => {
        const result = USER_DATA.find((ittem) => +ittem.memberId === +memberID);
        setUserData(result);
    };

    const onSubmit = (data) => {
        setVisible(false);
    };

    const handleAddMember = () => {
        setVisible(true);
    };

    const handelClose = () => {
        setVisible(false);
    };

    const handleRoute = (id) => {
        router.push(`/member/${id}`);
    };

    return (
        <MemberComponentStyled>
            <div className="card-member-search">
                <div className="text-center">
                    <Input label="ค้นหาสมาชิก" placeholder="รหัสสมาชิก/ชื่อ/นามสกุล/บัตรประชาชน/เบอร์โทร" />
                    <Btn buttonClassName={'w-[100%] md:w-[40%]'} secondary>
                        ค้นหา
                    </Btn>
                </div>
            </div>

            <div className="card-member-create">
                <div className="flex flex-row justify-end" onClick={handleAddMember}>
                    <Btn buttonClassName={'w-[100%]'} primary>
                        เพิ่มสายงาน
                    </Btn>
                </div>
            </div>

            <div className="card-member-details">
                <div className="text-sm text-[#7A7A7A]">
                    <div className="block">
                        <div className="flex flex-row justify-end">
                            <span>GP : </span>
                            <span className="ml-1 font-semibold">100</span>
                        </div>
                    </div>

                    <div className="block">
                        <div className="flex flex-row justify-end">
                            <span>TEAM GP :</span>
                            <span className="ml-1 font-semibold">100</span>
                        </div>
                    </div>
                </div>

                <MemberContainer>
                    <MemberItem disabled name={`${userData.first_name} ${userData.last_name}`} memberId={userData.memberId} active={userData.enable} />
                </MemberContainer>

                <Divider></Divider>

                <div className="flex flex-row flex-wrap mt-3">
                    {USER_DATA.map((item, index) => {
                        if (item?.upline_id?.includes(userData?.memberId) && userData.member_type === 'user') {
                            return (
                                <MemberContainer key={`memberItem_${index}`}>
                                    <MemberItem
                                        name={`${item.first_name} ${item.last_name}`}
                                        memberId={item.memberId}
                                        active={item.enable}
                                        onClick={() => handleRoute(item.memberId)}
                                    />
                                </MemberContainer>
                            );
                        }
                    })}

                    {userData.member_type === 'admin' &&
                        USER_DATA.map((item, index) => {
                            return (
                                <MemberContainer key={`memberItem_${index}`}>
                                    {item.memberId !== userData.memberId &&
                                        <MemberItem
                                            name={`${item.first_name} ${item.last_name}`}
                                            memberId={item.memberId}
                                            active={item.enable}
                                            onClick={() => handleRoute(item.memberId)}
                                        />
                                    }
                                </MemberContainer>
                            );
                        })}
                </div>
            </div>

            <Modal visible={visible} handleClose={handelClose}>
                <div className="flex justify-center mb-2 text-xl">เพิ่มสายงาน</div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Controller
                        name="firstName"
                        {...register('firstName', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="ชื่อ*" error={errors.firstName} />}
                    />
                    <Controller
                        name="lastname"
                        {...register('lastname', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="นามสกุล*" error={errors.lastname} />}
                    />
                    <Controller
                        name="idCard"
                        {...register('idCard', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="รหัสบัตรประชาชน*" error={errors.idCard} />}
                    />
                    <Controller
                        name="phone"
                        {...register('phone', { required: true })}
                        control={control}
                        render={({ field }) => <Input {...field} placeholder="เบอร์โทรศัพท์*" error={errors.phone} />}
                    />
                    <Controller
                        name="dayOfBirth"
                        {...register('dayOfBirth', { required: false })}
                        control={control}
                        render={({ field }) => <DatePickerMain {...field} placeholder="วันเกิด" />}
                    />

                    <div className="flex justify-center ">
                        <Btn type="submit" buttonClassName={'w-[100%] mt-4'} secondary>
                            บันทึก
                        </Btn>
                    </div>
                </form>
            </Modal>
        </MemberComponentStyled>
    );
}

export default MemberComponent;
