import styled from 'styled-components';

const IndexComponentStyled = styled.div`
    user-select: none;
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
    .card-details-previus {
        border-radius: 15px;
        min-height: 20 rem;
        padding: 1rem 1.2rem;
        background: #fff;
        margin-top: 1.3rem;
        border: 3px solid red;
        margin-left: 2rem;
        margin-right: 2rem;

        .disable {
            color: #9e9e9e;
        }

        .time {
            color: #6a6a6a;
            font-weight: 600;
        }

        .time-warning {
            color: orange;
            font-weight: 600;
        }

        .time-danger {
            color: #ff0b0b;
            font-weight: 600;
        }

        .point-gp {
            font-weight: 600;
            color: #6a6a6a;
        }

        .name {
            font-weight: 600;
        }

        .member-id {
            font-weight: 400;
            color: #9e9e9e;
            font-size: 14px;
            margin-top: -4px;
        }

        .label-balance {
            font-weight: 600;
            font-size: 12px;
            color: #6a6a6a;
        }

        .balance {
            font-family: 'roboto';
            font-weight: 600;
            color: #000;
            font-size: 36px;

            .thb {
                font-weight: 200;
                font-size: 36px;
                color: #454545;
            }
        }

        .status {
            font-weight: 500;
            color: #6a6a6a;

            .dot-status {
                font-size: 20px;
                color: green;
            }
        }

        .more {
            color: #6a6a6a;
            font-size: 14px;
            text-decoration: underline;
            cursor: pointer;
            display: flex;
            align-items: center;
        }

        .show-more {
            color: #6a6a6a;
            font-size: 14px;
        }
    }
    .card-details {
        border-radius: 15px;
        min-height: 20 rem;
        padding: 1rem 1.2rem;
        background: #fff;
        margin-top: 1.3rem;
        margin-left: 2rem;
        margin-right: 2rem;

        .disable {
            color: #9e9e9e;
        }

        .time {
            color: #6a6a6a;
            font-weight: 600;
        }

        .time-warning {
            color: orange;
            font-weight: 600;
        }

        .time-danger {
            color: #ff0b0b;
            font-weight: 600;
        }

        .point-gp {
            font-weight: 600;
            color: #6a6a6a;
        }

        .name {
            font-weight: 600;
        }

        .member-id {
            font-weight: 400;
            color: #9e9e9e;
            font-size: 14px;
            margin-top: -4px;
        }

        .label-balance {
            font-weight: 600;
            font-size: 12px;
            color: #6a6a6a;
        }

        .balance {
            font-family: 'roboto';
            font-weight: 600;
            color: #000;
            font-size: 36px;

            .thb {
                font-weight: 200;
                font-size: 36px;
                color: #454545;
            }
        }

        .status {
            font-weight: 500;
            color: #6a6a6a;

            .dot-status {
                font-size: 20px;
                color: green;
            }
        }

        .more {
            color: #6a6a6a;
            font-size: 14px;
            text-decoration: underline;
            cursor: pointer;
            display: flex;
            align-items: center;
        }

        .show-more {
            color: #6a6a6a;
            font-size: 14px;
        }
    }
`;

export default IndexComponentStyled;
