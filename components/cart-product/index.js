import React, { forwardRef } from 'react';
import CartProduct from './CartProduct';
import CartComponentStyled, { CardCart, CardCheckout } from './styled';
import { FaShoppingBasket } from 'react-icons/fa';
import { HiCreditCard } from 'react-icons/hi';
import { useState } from 'react';
import Currency from 'react-currency-formatter';
import { useDispatch, useSelector } from 'react-redux';
import { selectItems, selectTotal } from '../../features/cart/cartSlice';
import { useRouter } from 'next/router';
import { NumericFormat } from 'react-number-format';

const OrderComponent = forwardRef((props, ref) => {
    const items = useSelector(selectItems);
    const total = useSelector(selectTotal);
    const dispatch = useDispatch();
    const [disabled, setDisabled] = useState(false);
    const router = useRouter();

    const createCheckoutSession = async () => {
        router.push('/checkout');
        // setDisabled(true);
        // try {
        //   const stripe = await stripePromise;
        //   //call the backend to create a checkout session
        //   const checkoutSession = await axios.post("/api/create-checkout-session", {
        //     items: items,
        //     email: session.user.email,
        //   });;

        //   //Redirect user/customer to Stripe Checkout
        //   const result = await stripe.redirectToCheckout({
        //     sessionId: checkoutSession.data.id,
        //   });

        //   if (result.error) {
        //     alert(result.error.message);
        //     console.error(result.error.message);
        //   }
        // } catch (err) {
        //   console.error(err);
        //   alert(err);
        // }
        // setDisabled(false);
    };

    return (
        <CartComponentStyled>
            <CardCart>
                <div className="underline font-bold">ตะกร้าของฉัน</div>
                {items.map((item, index) => (
                    <CartProduct
                        key={`cart-product-${item?.id}`}
                        _id={item?.id}
                        title={item?.title}
                        price={item?.price}
                        description={item?.description}
                        category={item?.category}
                        image={item?.image}
                        qty={item?.qty}
                        border={index !== items?.length - 1}
                        disabled={disabled}
                    />
                ))}

                {items.length === 0 ? (
                    <div className="text-xl font-bold text-gray-400 my-auto h-[170px] text-center flex justify-center">
                        <div className="self-center">
                            <div className="justify-center">
                                ไม่มีสินค้าในตะกร้า <FaShoppingBasket className="mx-auto" size={50} />
                            </div>
                        </div>
                    </div>
                ) : (
                    <>
                        {items?.length ? (
                            <div className="flex flex-col bg-white md:p-10  py-8 px-6 shadow-md rounded-md md:text-xl sm:text-lg text-base my-10">
                                <h2 className="whitespace-nowrap font-semibold overflow-x-auto hideScrollBar">
                                    Subtotal ({items.length} items) :
                                    <span className="font-bold text-red-500 mx-2">
                                        <Currency quantity={total} currency="THB" />
                                    </span>
                                </h2>
                                {/* {session && ( */}
                                <button
                                    role="link"
                                    className={`button mt-6 flex items-center bg-[#0c4271] rounded-lg text-white justify-center lg:text-lg text-base  py-2 ${
                                        disabled ? 'opacity-50' : ''
                                    }`}
                                    onClick={!disabled ? createCheckoutSession : () => {}}
                                    disabled={disabled}>
                                    <HiCreditCard className="sm:w-6 w-5" fontSize={'25px'} />
                                    <span className="ml-2">Proceed to checkout </span>
                                </button>
                                {/* )} */}
                            </div>
                        ) : (
                            <></>
                        )}
                    </>
                )}
            </CardCart>
        </CartComponentStyled>
    );
});

export default OrderComponent;
