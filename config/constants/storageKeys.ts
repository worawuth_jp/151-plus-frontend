export const storageKey = {
  isConnected: "connected",
  accessToken: "access_token",
  refreshToken: "refresh_token",
};

export const allowedProvider = ["facebook","google"]
 