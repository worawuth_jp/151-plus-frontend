import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import { storageKey } from '../config/constants/storageKeys';
import jwtHelper from '../utils/jwtHelper';
import localStorageService from '../services/localStorage.service';
import Loading from '../components/shared/Loading';
import { render } from 'react-dom';

export const withAuth = (Component) => {
    // const AuthenticateComponents = (props) => {
    //     const [renderComponent, setRenderComponent] = useState(<Loading isLoading={true} />);
    //     const [isLoading, setLoading] = useState(false);
    //     const whiteLists = (process.env.NEXT_PUBLIC_PAGE_WHITE_LISTS || '').split(',');
    //     const router = useRouter();
    //     const loadSession = async () => {
    //         let accessToken = localStorage.getItem(storageKey.accessToken);
    //         if (
    //             !whiteLists.includes(router.pathname) &&
    //             (accessToken === null || accessToken === undefined || accessToken === '')
    //         ) {
    //             location.href = '/login';
    //             setRenderComponent(<Loading isLoading={true} />);
    //             return <Loading isLoading={true} />;
    //         }

    //         if (!whiteLists.includes(router.pathname) && jwtHelper.expireToken(accessToken)) {
    //             await localStorageService.clearItem();
    //             location.href = '/login';
    //             setRenderComponent(<Loading isLoading={true} />);
    //             return <Loading isLoading={true} />;
    //         } else {
    //             setRenderComponent(<Component {...props} />);
    //             return <Component {...props} />;
    //         }
    //     };

    //     useEffect(() => {
    //         loadSession();
    //         setLoading(true);
    //     }, [isLoading]);

    //     return renderComponent;
    // };

    const AuthenticatedComponent = (props) => {
        const router = useRouter();
        const [isOpen, setOpen] = useState(false);

        useEffect(() => {
            const auth = async () => {
                let accessToken = localStorage.getItem(storageKey.accessToken);
                const whiteLists = (process.env.NEXT_PUBLIC_PAGE_WHITE_LISTS || '').split(',');

                if (whiteLists.includes(router.pathname)) {
                    setOpen(true);
                    return <Component {...props} />;
                }

                if (
                    !whiteLists.includes(router.pathname) &&
                    (accessToken === null || accessToken === undefined || accessToken === '')
                ) {
                    setOpen(false);
                    location.href = '/login';
                    return <Loading isLoading={true} />;
                }

                if (!whiteLists.includes(router.pathname) && jwtHelper.expireToken(accessToken)) {
                    await localStorageService.clearItem();
                    setOpen(false);
                    location.href = '/login';
                    return <Loading isLoading={true} />;
                }
                setOpen(true);
            };

            auth();
        }, []);

        return !isOpen ? <Loading isLoading={true} /> : <Component {...props} />; // Render whatever you want while the authentication occurs
    };

    return AuthenticatedComponent;
};
