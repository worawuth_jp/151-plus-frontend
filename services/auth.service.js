import httpClient from './httpClient';

const register = async (body) => {
    const url = `member/register`;
    const data = await httpClient().post(url, body);
    return data;
};

const login = async (body) => {
    const url = `member/login`;
    const data = await httpClient().post(url, body);
    return data;
};

const resetPassword = async (body) => {
    const url = `member/repassword`;
    const data = await httpClient().put(url, body);
    return data;
};

const authService = {
    login,
    register,
    resetPassword,
};

export default authService;
