import httpClient from './httpClient';

const getUserInfo = async (id) => {
    const url = `member/info/${id}`;
    const data = await httpClient().get(url);
    return data;
};

const updateDocument = async (body) => {
    const url = `member/document/send`;
    const data = await httpClient().post(url, body);
    return data;
};

const updateMember = async (body) => {
    const url = `member/update`;
    const data = await httpClient().put(url, body);
    return data;
};

const getDownline = async () => {
    const url = `member/downline`;
    const data = await httpClient().get(url);
    return data;
};

const getDocumentByAdmin = async (id) => {
    const url = `admin/get-all-document/${id}`;
    const data = await httpClient().get(url);
    return data;
};

const updateStatusMemberByAdmin = async (name) => {
    const url = `admin/${name}`;
    const data = await httpClient().delete(url);
    return data;
};

const deleteMember = async (id) => {
    const url = `member/${id}`;
    const data = await httpClient().delete(url);
    return data;
};

const memberService = {
    getUserInfo,
    updateDocument,
    updateMember,
    getDownline,
    getDocumentByAdmin,
    updateStatusMemberByAdmin,
    deleteMember
};

export default memberService;
