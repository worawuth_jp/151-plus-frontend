import httpClient from './httpClient';

const createOrder = async (body) => {
    const url = `order/create-order`;
    const data = await httpClient().post(url, body);
    return data;
};

const updateOrder = async (id, body) => {
    const url = `order/update-order/${id}`;
    const data = await httpClient().put(url, body);
    return data;
};

const deleteOrder = async (id) => {
    const url = `order/${id}`;
    const data = await httpClient().delete(url);
    return data;
};

const updateOrderStatus = async () => {
    const url = `order/update-order-status`;
    const data = await httpClient().put(url);
    return data;
};

const getMyOrderList = async (id) => {
    const url = `order`;
    const data = await httpClient().get(url);
    return data;
};

const getAllOrderList = async (id) => {
    const url = `product/${id}`;
    const data = await httpClient().get(url);
    return data;
};

const orderService = {
    createOrder,
    updateOrder,
    deleteOrder,
    updateOrderStatus,
    getMyOrderList,
    getAllOrderList,
};

export default orderService;
