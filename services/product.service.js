import httpClient from './httpClient';

const createProduct = async (body) => {
    const url = `product`;
    const data = await httpClient().post(url, body);
    return data;
};

const updateProduct = async (id, body) => {
    const url = `product/${id}`;
    const data = await httpClient().put(url, body);
    return data;
};

const deleteProduct = async (id) => {
    const url = `product/${id}`;
    const data = await httpClient().delete(url);
    return data;
};

const getListProduct = async () => {
    const url = `product`;
    const data = await httpClient().get(url);
    return data;
};

const getProductById = async (id) => {
    const url = `product/${id}`;
    const data = await httpClient().get(url);
    return data;
};

const getProductImage = async (id) => {
    const url = `uploads/repassword`;
    const data = await httpClient().get(url, body);
    return data;
};

const getAllProductByAdmin = async (id) => {
    const url = `member/repassword`;
    const data = await httpClient().get(url, body);
    return data;
};

const updateImage = async (id) => {
    const url = `member/repassword`;
    const data = await httpClient().get(url, body);
    return data;
};

const productService = {
    createProduct,
    updateProduct,
    deleteProduct,
    getListProduct,
    getProductById,
    getProductImage,
    getAllProductByAdmin,
    updateImage,
};

export default productService;
