import { storageKey } from '../config/constants/storageKeys';
import axios from 'axios';
import localStorageService from './localStorage.service';

const url = `${process.env.NEXT_PUBLIC_BASE_API}` || '';
const apiKey = `${process.env.NEXT_PUBLIC_API_KEY}` || '';

const httpClient = (baseURL = url) => {
    const http = axios.create({
        baseURL,
    });

    http.interceptors.request.use(async (config) => {
        // set api key
        config.headers['x-api-key'] = apiKey;

        // header authorization
        const accessToken = localStorageService.getItem(storageKey.accessToken);
        if (accessToken) {
            config.headers.authorization = `Bearer ${accessToken}`;
        }
        return config;
    });

    http.interceptors.response.use(
        (response) => response,
        async (error) => {
            if (error.response) {
                if (error.response.status === 401) {
                    return Promise.reject(error);
                } else if (error?.response?.status === 400) {
                    return Promise.reject(error);
                } else {
                    return Promise.reject(error);
                }
            }
        }
    );
    return http;
};

export default httpClient;
