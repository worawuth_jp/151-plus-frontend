FROM node:16
WORKDIR /usr/src/app
COPY package.json ./

RUN npm install --force
RUN npm install react-scripts -g

COPY . .
COPY .env.dev .env
EXPOSE 3000
CMD ["npm", "run", "dev"]