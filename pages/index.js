import IndexComponent from '../components/index';
import ContentLayout from '../components/layouts/ContentLayout';
import IndexStyled from './index/styled';

export default function Home() {
    return (
        <IndexStyled>
            <ContentLayout>
                <IndexComponent />
            </ContentLayout>
        </IndexStyled>
    );
}
