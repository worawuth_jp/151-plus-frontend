import React from 'react';
import ContentLayout from '../../../components/layouts/ContentLayout';
import MemberComponent from '../../../components/member';
import MemberPageStyled from './styled';

function MemberPage({ ...props }) {
    return (
        <MemberPageStyled>
            <ContentLayout>
                <MemberComponent />
            </ContentLayout>
        </MemberPageStyled>
    );
}

export default MemberPage;
