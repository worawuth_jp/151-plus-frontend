import React from 'react';

// layout
import ContentLayout from '../../components/layouts/ContentLayout';
import IncomeDetailComponent from '../../components/income-detail';
import IncomeDetailPageStyled from './styled';

function IncomeDetailPage() {
    return (
        <IncomeDetailPageStyled>
            <ContentLayout>
                <IncomeDetailComponent />
            </ContentLayout>
        </IncomeDetailPageStyled>
    );
}

export default IncomeDetailPage;
