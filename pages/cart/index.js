import React from 'react';
import CartComponent from '../../components/cart-product';
import ContentLayout from '../../components/layouts/ContentLayout';
import CartStyled from './styled';

function Cartage() {
    return (
        <CartStyled>
            <ContentLayout>
                <CartComponent />
            </ContentLayout>
        </CartStyled>
    );
}

export default Cartage;
