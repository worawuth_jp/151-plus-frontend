import { useRouter } from 'next/router';
import { useEffect } from 'react';
import Loading from '../components/shared/Loading';
import localStorageService from '../services/localStorage.service';

const Logout = () => {
    const router = useRouter();
    useEffect(() => {
        // window.localStorage.clear();
        localStorageService.clearItem();
        router.push('/login');
    }, []);
    return (
        <>
            <Loading isLoading={true} />
        </>
    );
};

export default Logout;
