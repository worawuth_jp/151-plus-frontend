import { Fragment } from 'react';
import GlobalStyle from '../styles/globals';
import '../styles/globals.css';
import { Provider } from 'react-redux';
import { store } from '../app/store';

// react-toastify
import { ToastContainer } from 'react-toastify'; //styles of nprogress
import 'react-toastify/dist/ReactToastify.css';
import { withAuth } from '../hoc/withAuth';

function MyApp({ Component, pageProps }) {
    return (
        <Provider store={store}>
            <Fragment>
                <GlobalStyle />
                <Component {...pageProps} />
                <ToastContainer limit={4} />
            </Fragment>
        </Provider>
    );
}

export default withAuth(MyApp);
