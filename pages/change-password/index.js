import React from 'react';
import ChangePasswordComponent from '../../components/change-password';
import ContentLayout from '../../components/layouts/ContentLayout';
import ChangePasswordStyled from './styled';

function ChangePasswordPage() {
    return (
        <ChangePasswordStyled>
            <ContentLayout>
                <ChangePasswordComponent />
            </ContentLayout>
        </ChangePasswordStyled>
    );
}

export default ChangePasswordPage;
