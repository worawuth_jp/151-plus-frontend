import React from 'react';
import ContentLayout from '../../components/layouts/ContentLayout';
import ProductComponent from '../../components/product';
import ProductPageStyled from './styled';

function ProductPage() {
    return (
        <ProductPageStyled>
            <ContentLayout>
                <ProductComponent />
            </ContentLayout>
        </ProductPageStyled>
    );
}

export default ProductPage;
