import React from 'react';
// layout
import ContentLayout from '../../../components/layouts/ContentLayout';

import HistoryComponent from '../../../components/history';
import HistoryByMemberIdPageStyled from './styled';

function HistoryByMemberPage() {
    return (
        <HistoryByMemberIdPageStyled>
            <ContentLayout>
                <HistoryComponent />
            </ContentLayout>
        </HistoryByMemberIdPageStyled>
    );
}

export default HistoryByMemberPage
;
