import React from 'react';
import LoginLayout from '../../components/layouts/LoginLayout';
import LoginComponent from '../../components/login';
import LoginPageStyle from './styled';

function LoginPage() {
    return (
        <LoginPageStyle>
            <LoginLayout>
                <LoginComponent />
            </LoginLayout>
        </LoginPageStyle>
    );
}

export default LoginPage;
