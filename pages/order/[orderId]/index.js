import React from 'react';
import ContentLayout from '../../../components/layouts/ContentLayout';
import OrderComponent from '../../../components/orders/OrderDetail';
import OrderByMemberIdPageStyled from './styled';

function OrderByMemberPage() {
    return (
        <OrderByMemberIdPageStyled>
            <ContentLayout>
                <OrderComponent />
            </ContentLayout>
        </OrderByMemberIdPageStyled>
    );
}

export default OrderByMemberPage;
