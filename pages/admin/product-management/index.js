import React, { forwardRef } from 'react';
import CheckoutComponent from '../../../components/admin/product-management';
import ContentLayout from '../../../components/layouts/ContentLayout';
import Checkoutyled from './styled';

const ProductManagePage = forwardRef((props, ref) => {
    return (
        <Checkoutyled>
            <ContentLayout>
                <CheckoutComponent />
            </ContentLayout>
        </Checkoutyled>
    );
});

export default ProductManagePage;
