import React, { forwardRef } from 'react';
import CheckoutComponent from '../../../components/admin/report-management';
import ContentLayout from '../../../components/layouts/ContentLayout';
import Checkoutyled from './styled';

const reportManagePage = forwardRef((props, ref) => {
    return (
        <Checkoutyled>
            <ContentLayout>
                <CheckoutComponent />
            </ContentLayout>
        </Checkoutyled>
    );
});

export default reportManagePage;
