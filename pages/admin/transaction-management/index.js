import React, { forwardRef } from 'react';
import CheckoutComponent from '../../../components/admin/transaction-management';
import ContentLayout from '../../../components/layouts/ContentLayout';
import Checkoutyled from './styled';

const TransactionManagePage = forwardRef((props, ref) => {
    return (
        <Checkoutyled>
            <ContentLayout>
                <CheckoutComponent />
            </ContentLayout>
        </Checkoutyled>
    );
});

export default TransactionManagePage;
