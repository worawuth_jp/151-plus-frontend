import React, { forwardRef } from 'react';
import CheckoutComponent from '../../../components/admin/member-management';
import ContentLayout from '../../../components/layouts/ContentLayout';
import Checkoutyled from './styled';

const MembermanagePage = forwardRef((props, ref) => {
    return (
        <Checkoutyled>
            <ContentLayout>
                <CheckoutComponent />
            </ContentLayout>
        </Checkoutyled>
    );
});

export default MembermanagePage;
