import React, { forwardRef } from 'react';
import CheckoutComponent from '../../../components/admin/order-management';
import ContentLayout from '../../../components/layouts/ContentLayout';
import Checkoutyled from './styled';

const OrderManagePage = forwardRef((props, ref) => {
    return (
        <Checkoutyled>
            <ContentLayout>
                <CheckoutComponent />
            </ContentLayout>
        </Checkoutyled>
    );
});

export default OrderManagePage;
