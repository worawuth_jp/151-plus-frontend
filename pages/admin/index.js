import AdminIndexComponent from '../../components/admin';
import ContentLayout from '../../components/layouts/ContentLayout';
import IndexStyled from './index/styled';

function Admin() {
    return (
        <IndexStyled>
            <ContentLayout>
                <AdminIndexComponent />
            </ContentLayout>
        </IndexStyled>
    );
}

Admin.admin = true;
export default Admin;
