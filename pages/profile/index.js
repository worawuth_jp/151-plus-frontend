import React from 'react';
import ContentLayout from '../../components/layouts/ContentLayout';
import ProfileComponent from '../../components/profile';
import ProfilePageStyled from './styled';

function ProductPage() {
    return (
        <ProfilePageStyled>
            <ContentLayout>
                <ProfileComponent />
            </ContentLayout>
        </ProfilePageStyled>
    );
}

export default ProductPage;
