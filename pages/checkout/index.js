import React from 'react';
import CheckoutComponent from '../../components/checkout';
import ContentLayout from '../../components/layouts/ContentLayout';
import Checkoutyled from './styled';

function Checkoutage() {
    return (
        <Checkoutyled>
            <ContentLayout>
                <CheckoutComponent />
            </ContentLayout>
        </Checkoutyled>
    );
}

export default Checkoutage;
