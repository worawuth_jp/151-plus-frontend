import jwt from "jsonwebtoken";
import { storageKey } from "../config/constants/storageKeys";

const expireToken = (_token) => {
    const accessToken = localStorage.getItem(storageKey.accessToken);
    if (accessToken) {
        let tokenDecode = {};
        try {
            tokenDecode = jwt.decode(
                accessToken.substring(1, accessToken.length - 1)
            );
            const dateNow = Date.now();
            const expiredDate = tokenDecode.exp * 1000;
            return expiredDate <= dateNow;
        } catch (err) {
            console.log(err);
        }
    }
    return true;
};

const hasRole = () => {
    const accessToken = localStorage.getItem(storageKey.accessToken);
    if (accessToken) {
        const tokenDecode = jwt.decode(accessToken, {
            complete: true,
        });
        const roles = tokenDecode.role;
        return roles;
    }
    return false;
};

const getUsernameByToken = () => {
    const accessToken = localStorage.getItem(storageKey.accessToken);
    if (accessToken) {
        const tokenDecode = jwt.decode(
            accessToken.substring(1, accessToken.length - 1), {
                complete: true,
            }
        );
        return tokenDecode;
    }
};

const jwtHelper = { expireToken, hasRole, getUsernameByToken };

export default jwtHelper;